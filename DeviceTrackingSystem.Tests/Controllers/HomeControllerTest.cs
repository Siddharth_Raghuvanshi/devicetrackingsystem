﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeviceTrackingSystem;
using DeviceTrackingSystem.Controllers;

namespace DeviceTrackingSystem.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
           // Assert.AreEqual(" ", result.ViewBag.Message);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Customers() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Devices() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
         [TestMethod]
        public void Patients()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Patients() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
