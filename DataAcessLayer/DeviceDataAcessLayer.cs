﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
//using DataAcessLayer;
using DataAcessLayer;
//using DataAcessLayer.DeviceAccess;


namespace DataAcessLayer
{
    public class DeviceDataAcessLayer
    {
        string connectinstring = ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString);
        
        public IEnumerable<DeviceAccess> DeviceData
        {
            get
            {
    
                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetAllDevices", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.Part_ID = rdr["Part_ID"].ToString().Trim();
                        device.Serial_Number = rdr["Serial_Number"].ToString().Trim();
                        device.Status = rdr["Status"].ToString().Trim();
                        device.ID = Convert.ToInt32(rdr["ID"]);
                        device.Archived = Convert.ToBoolean(rdr["Archived"]);
                        if (!(rdr["Customer_ID"] is DBNull))
                        {
                            device.Customer_ID = Convert.ToInt32(rdr["Customer_ID"]);
                        }
                       
                        if (!(rdr["Parent_ID"] is DBNull))
                        {
                            device.Parent_ID = Convert.ToString(rdr["Parent_ID"]);
                        }
                        else
                        {
                            device.Parent_ID = string.Empty;
                        }
                        if (!(rdr["PID"] is DBNull))
                        {
                            device.PID = Convert.ToInt32(rdr["PID"]);
                        }
                        if (!(rdr["Customer_Name"] is DBNull))
                        {
                            device.Customer_Name = rdr["Customer_Name"].ToString().Trim();
                        }
                        else
                        {
                            device.Customer_Name = "-";
                        }

                        if (!(rdr["DeviceLocationId"] is DBNull))
                        {
                            device.DeviceLocationId = Convert.ToInt32(rdr["DeviceLocationId"]);
                        }

                       
                        device.ParentSerialNumber = rdr["Parent Serial Number"].ToString().Trim();
                        devices.Add(device);
                    }
                }
                return devices;
            }
        }

        public IEnumerable<DeviceAccess> DeviceDataDistinctParentID
        {
            get
            {
       
                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetDistinctParentID", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        
                       
                        if (!(rdr["Parent_ID"] is DBNull))
                        {
                            device.Parent_ID = rdr["Parent_ID"].ToString();
                          
                        }
                        devices.Add(device);
                    }
                }
                return devices;
            }
        }

        public IEnumerable<DeviceAccess> CustomerDevice
        {
            get
            {
   

                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetAllCustomerDetails", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();

                        device.ID = Convert.ToInt32(rdr["ID"]);
                        device.ExpandableCustomerID = rdr["Expandable CustomerID"].ToString().Trim();
                        device.ExpandableCustomerName = rdr["Expandable Customer Name"].ToString().Trim();
                        device.SalesForceCustomerName = rdr["Sales Force Customer Name"].ToString().Trim();
                        device.SF_AccountID = Convert.ToInt32(rdr["SF_AccountsID"]);
                        device.Updated = Convert.ToDateTime(rdr["Updated"]);
                        device.Last_Altered_By = rdr["Last_Updated_By"].ToString().Trim();
                        device.IsDeleted = Convert.ToBoolean(rdr["IsDeleted"]);
                        device.Archived = Convert.ToBoolean(rdr["Archived"]);
                     
                        devices.Add(device);

                        
                    }

                }
                return devices;

            }

        }

        public IEnumerable<DeviceAccess> PatientDevice
        {
            get
            {
  

                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetAllPatientDetails", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.ID = Convert.ToInt32(rdr["ID"]);
                        device.PatientID = rdr["PatientID"].ToString();
                        device.Customer_Name = rdr["Customer_Name"].ToString();
                        device.ImplantPhysician = rdr["Implant_Physcian"].ToString();
                        if (!(rdr["ImplantDate"] is DBNull))
                        {
                            var impdate = Convert.ToDateTime(rdr["ImplantDate"]);
                            device.PatientsImplantDate = impdate.ToShortDateString();
                          
                        }
                        else
                        {
                            device.PatientsImplantDate = "-";
                        }

                     

                        
                        if (!(rdr["Explant_Date"] is DBNull))
                        {
                            var expdate = Convert.ToDateTime(rdr["Explant_Date"]);
                            device.PatientsDeathDate = expdate.ToShortDateString();
                            
                        }
                        else
                        {
                            device.PatientsDeathDate = "-";
                        }
                        device.PatientsImplantPhysician = rdr["IRF"].ToString();
                        device.Last_Altered_By = rdr["LastUpdatedBy"].ToString();
                        if (!(rdr["Customer_ID"] is DBNull))
                        {
                            device.Customer_ID = Convert.ToInt32(rdr["Customer_ID"]);
                        }
                     
                        devices.Add(device);
                    }

                }
                return devices;

            }

        }

        public IEnumerable<DeviceAccess> PatientDeviceDetail
        {
            get
            {     
                List<DeviceAccess> devices = new List<DeviceAccess>();
                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetAllPatientDetails", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.ID = Convert.ToInt32(rdr["ID"]);
                        device.PatientID = rdr["PatientID"].ToString();
                        device.PatientsImplantDate = Convert.ToString(rdr["ImplantDate"]);
                        device.PatientsDeathDate = Convert.ToString(rdr["Explant_Date"]);
                        device.PatientsImplantPhysician = rdr["IRF"].ToString();

                        if (!(rdr["Customer_ID"] is DBNull))
                        {
                            device.Customer_ID = Convert.ToInt32(rdr["Customer_ID"]);
                        }
                        devices.Add(device);
                    }
                }
                return devices;
            }
        }       

        public IEnumerable<DeviceAccess> DeviceTrackingLogHome
        {
            get
            {    

                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetDeviceTrackingLogHome", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.LogId = Convert.ToInt32(rdr["LogId"]);
                        device.LogDescription = rdr["LogDescription"].ToString();
                        device.Source = rdr["Source"].ToString();
                        device.StartDateTime = Convert.ToDateTime(rdr["StartDateTime"]);
                        device.EndDateTime = Convert.ToDateTime(rdr["EndDateTime"]);
                        device.LogStatus = Convert.ToString(rdr["Status"]);
                        device.LogDate = Convert.ToDateTime(rdr["LogDate"]);
                        device.LoggedBy = rdr["LoggedBy"].ToString();
                        devices.Add(device);
                    }

                }
                return devices;

            }

        }

        public class Status
        {           
            public DataSet BindStatus()
            {
                DeviceAccess db = new DeviceAccess();
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString);
                SqlCommand cmd = new SqlCommand("sp_GetStatusData", con);                
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
        }

        public class ParentPartID
        {          
            public DataSet BindParentPartID()
            {
                DeviceAccess db = new DeviceAccess();
                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString);
                SqlCommand cmd = new SqlCommand("sp_GetParentPart_ID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
        }

        public bool DeleteDeviceRecord(int id, string updatedby)
        {
            bool IsDeleted = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "DeleteDevices";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.Parameters.AddWithValue("@Updatedby", updatedby);

                    cmd.Parameters.Add("@isDeleted", SqlDbType.Bit);
                    cmd.Parameters["@isDeleted"].Direction = ParameterDirection.Output;

                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        IsDeleted = Convert.ToBoolean(cmd.Parameters["@IsDeleted"].Value);
                    }
                    con.Close();
                }
            }
            return IsDeleted;
        }
    

        public bool DeleteCustomerRecord(string id,string updatedby)
        {
            bool IsDeleted = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "DeleteCustomers";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@CustomerID", id);
                    cmd.Parameters.AddWithValue("@Updatedby", updatedby);

                    cmd.Parameters.Add("@isDeleted", SqlDbType.Bit);
                    cmd.Parameters["@isDeleted"].Direction = ParameterDirection.Output;

                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        IsDeleted = Convert.ToBoolean(cmd.Parameters["@IsDeleted"].Value);
                    }
                    con.Close();
                }
            }
            return IsDeleted;
        }


        public bool DeletePatientRecord(int id, string updatedby)
        {
            bool IsDeleted = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "DeletePatient";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@ID", id);
                    cmd.Parameters.AddWithValue("@Updatedby", updatedby);

                    cmd.Parameters.Add("@isDeleted", SqlDbType.Bit);
                    cmd.Parameters["@isDeleted"].Direction = ParameterDirection.Output;

                    con.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        IsDeleted = Convert.ToBoolean(cmd.Parameters["@IsDeleted"].Value);
                    }
                    con.Close();
                }
            }
            return IsDeleted;
        }

        public bool SaveDevices(string status, int parentPartId, string parentSerialNumber, int customerId, int deviceId, int deviceLocationId, string updatedby,string archived)
        {
            bool isUpdated = false;        

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateDeviceDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@ParentPartId", parentPartId);
                cmd.Parameters.AddWithValue("@ParentSerialNumber", parentSerialNumber);
                cmd.Parameters.AddWithValue("@CustomerId", customerId);
                cmd.Parameters.AddWithValue("@DeviceId", deviceId);
                cmd.Parameters.AddWithValue("@DeviceLocationId", deviceLocationId);
                cmd.Parameters.AddWithValue("@updatedby", updatedby);
                cmd.Parameters.AddWithValue("@archived", archived);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }


        public bool SavePatients(int customerId, string Implantdate, string ImplantPhysician, string ExplantDate, int PatientId, string updatedby)
        {
            bool isUpdated = false;
         

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdatePatientDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Customer_ID", customerId);
                cmd.Parameters.AddWithValue("@Implant_Date", Implantdate);
                cmd.Parameters.AddWithValue("@Implant_Physcian", ImplantPhysician);
                cmd.Parameters.AddWithValue("@Explant_Date", ExplantDate);
                cmd.Parameters.AddWithValue("@PatientsId", PatientId);             
                cmd.Parameters.AddWithValue("@updatedby", updatedby);
            
                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }


        public bool SaveCustomers(int SFAccountId, int customerId, string updatedby, string archived)
        {
            bool isUpdated = false;         

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateCustomerDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Account_ID", SFAccountId);
                cmd.Parameters.AddWithValue("@CustomerId", customerId);             
                cmd.Parameters.AddWithValue("@updatedby", updatedby);
                cmd.Parameters.AddWithValue("@archived", archived);
                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }


        public bool SaveCustomersDevices(int PartID, int customerId,int DeviceId, string updatedby)
        {
            bool isUpdated = false;         

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateCustomerDevicesDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Part_ID", PartID);
                cmd.Parameters.AddWithValue("@CustomerId", customerId);
                cmd.Parameters.AddWithValue("@DeviceId", DeviceId);               
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }


        public bool SaveCustomerPatients(int NewPatientID, int customerId, int PatientId, string updatedby)
        {
            bool isUpdated = false;         

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateCustomerPatientDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@NewPatientID", NewPatientID);
                cmd.Parameters.AddWithValue("@CustomerId", customerId);
                cmd.Parameters.AddWithValue("@PatientId", PatientId);
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }

        public bool SavePatientChildDetails(int NewPartID, int DeviceId, int PatientId, string updatedby)
        {
            bool isUpdated = false;
          
            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdatePatientChildDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@NewPartID", NewPartID);
                cmd.Parameters.AddWithValue("@PatientId", PatientId);
                cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }

        public bool SaveDevicesParentDevice(int NewPartID,int PartId, string updatedby)
        {
            bool isUpdated = false;
         
            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateDevicesParentDevice", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@NewPartID", NewPartID);
                cmd.Parameters.AddWithValue("@PartId", PartId);             
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }

        public bool SaveDevicesLocationHistoryDetails(int NewCustomerID, int DeviceId, int CustomerID, string updatedby)
        {
            bool isUpdated = false;
            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateDevicesLocationHistoryDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@NewCustomerID", NewCustomerID);
                cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID);
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }

        public bool SaveDevicesChildDetails(int NewCustomerID, int DeviceId, int CustomerID, string updatedby)
        {
            bool isUpdated = false;
          
            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateDevicesChildDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@NewCustomerID", NewCustomerID);
                cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID);
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return isUpdated;
        }

        public bool GetIsArchived(int CustomerID, int AccountId)
        {
            bool IsArchived = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("GetCustomerArchived", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CustomerId", CustomerID);
                cmd.Parameters.AddWithValue("@AccountId", AccountId);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;
                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    IsArchived = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return IsArchived;
        }

        public bool GetDeviceIsArchived(int partId)
        {
            bool IsArchived = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("GetDevicesArchived", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@partId", partId);
               
                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;
                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    IsArchived = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }

            return IsArchived;
        }

        public int GetParentIdByParentandSerialNum(string  parentName,string serialNum)
        {
            int pid= 0;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("GetParentIdByParentandSerialNum", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@parentName", parentName.Trim());
                cmd.Parameters.AddWithValue("@serialNum", serialNum.Trim());
                cmd.Parameters.Add("@pid", SqlDbType.Int);
                cmd.Parameters["@pid"].Direction = ParameterDirection.Output;
                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    pid = Convert.ToInt32(cmd.Parameters["@pid"].Value);
                }
                con.Close();
            }

            return pid;
        }

        public List<DataAcessLayer.DeviceAccess.DeviceDetails> GetSerialNumberByPartID(string id)
        {         

            List<DataAcessLayer.DeviceAccess.DeviceDetails> devices = new List<DataAcessLayer.DeviceAccess.DeviceDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("GetSerialNumberByPartID", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@partId", id);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.DeviceDetails device = new DataAcessLayer.DeviceAccess.DeviceDetails();
                  
                    device.Serial_Number = rdr["Serial_Number"].ToString();
                    if (!(rdr["PID"] is DBNull))
                    {
                        device.PID = Convert.ToInt32(rdr["PID"]);
                    }

                    devices.Add(device);
                }
            }
            return devices;
        }

        public List<DataAcessLayer.DeviceAccess.PatientsDetails> ParentPatientDetail()
        {

            List<DataAcessLayer.DeviceAccess.PatientsDetails> devices = new List<DataAcessLayer.DeviceAccess.PatientsDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("GetParentPatientDetail", con);
                cmd.CommandType = CommandType.StoredProcedure;               
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.PatientsDetails device = new DataAcessLayer.DeviceAccess.PatientsDetails();
                    device.ID = Convert.ToInt32(rdr["ID"]);
                    if (!(rdr["PatientID"] is DBNull))
                    {
                        device.Patient_ID = rdr["PatientID"].ToString().Trim();
                    }
                    if (!(rdr["Customer_Name"] is DBNull))
                    {
                        device.Customer_Name = rdr["Customer_Name"].ToString().Trim();
                    }
                    if (!(rdr["ImplantDate"] is DBNull))
                    {
                        device.PatientsImplantDate = Convert.ToDateTime(rdr["ImplantDate"]);
                    }
                    if (!(rdr["Explant_Date"] is DBNull))
                    {
                        device.PatientsDeathDate = Convert.ToDateTime(rdr["Explant_Date"]);
                    }
                    if (!(rdr["IRF"] is DBNull))
                    {
                        device.PatientsImplantPhysician = rdr["IRF"].ToString().Trim();
                    }
                    if (!(rdr["LastUpdatedBy"] is DBNull))
                    {
                        device.Last_Altered_By = rdr["LastUpdatedBy"].ToString().Trim();
                    }
                    if (!(rdr["Updated"] is DBNull))
                    {
                        device.Updated = rdr["Updated"].ToString();
                    }
                    devices.Add(device);
                }
                return devices;
            }
        }

        public List<DataAcessLayer.DeviceAccess.PatientsAssignedDevices> ChildPatientDetail()
        {
            
            List<DataAcessLayer.DeviceAccess.PatientsAssignedDevices> devices = new List<DataAcessLayer.DeviceAccess.PatientsAssignedDevices>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("GetChildPatientDetail", con);
                cmd.CommandType = CommandType.StoredProcedure;           
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.PatientsAssignedDevices device = new DataAcessLayer.DeviceAccess.PatientsAssignedDevices();
                    device.ID = Convert.ToInt32(rdr["ID"]);
                    if (!(rdr["Patient_ID"] is DBNull))
                    {
                        device.Patient_ID = Convert.ToInt32(rdr["Patient_ID"]);
                    }
                    if (!(rdr["Part_ID"] is DBNull))
                    {
                        device.Part_ID = rdr["Part_ID"].ToString().Trim();
                    }
                    if (!(rdr["Serial_Number"] is DBNull))
                    {
                        device.Serial_Number = rdr["Serial_Number"].ToString().Trim();
                    }
                    if (!(rdr["Status"] is DBNull))
                    {
                        device.Status = rdr["Status"].ToString().Trim();
                    }
                    if (!(rdr["ParentPartID"] is DBNull))
                    {
                        device.ParentPartID = rdr["ParentPartID"].ToString().Trim();
                    }
                    if (!(rdr["Parent Serial Number"] is DBNull))
                    {
                        device.Parent_Serial_Number = rdr["Parent Serial Number"].ToString().Trim();
                    }
                    if (!(rdr["Customer_ID"] is DBNull))
                    {
                        device.Customer_ID = (rdr["Customer_ID"]).ToString().Trim();
                    }
                    if (!(rdr["DeviceLocID"] is DBNull))
                    {
                        device.DeviceLocID = Convert.ToInt32(rdr["DeviceLocID"]);
                    }
                    if (!(rdr["PID"] is DBNull))
                    {
                        device.PID = Convert.ToInt32(rdr["PID"]);
                    }
                    devices.Add(device);
                }


                return devices;
            }
        }

        public List<DataAcessLayer.DeviceAccess.CustomersDetails> CustomerDetails(int id)
        {
          
            List<DataAcessLayer.DeviceAccess.CustomersDetails> devices = new List<DataAcessLayer.DeviceAccess.CustomersDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("CustomerDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CustID", id);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.CustomersDetails device = new DataAcessLayer.DeviceAccess.CustomersDetails();
                    if (!(rdr["Expandable CustomerID"] is DBNull))
                    {
                        device.CustomerID = rdr["Expandable CustomerID"].ToString().Trim();
                    }
                    if (!(rdr["Expandable Customer Name"] is DBNull))
                    {
                        device.CustomerName = rdr["Expandable Customer Name"].ToString().Trim();
                    }
                    if (!(rdr["Sales Force Customer Name"] is DBNull))
                    {
                        device.SalesForceAccount = rdr["Sales Force Customer Name"].ToString().Trim();
                    }
                    if (!(rdr["Updated"] is DBNull))
                    {
                        device.Updated = Convert.ToDateTime(rdr["Updated"]);
                    }
                    if (!(rdr["Last_Updated_By"] is DBNull))
                    {
                        device.LastAlteredBy = rdr["Last_Updated_By"].ToString().Trim();
                    }

                    devices.Add(device);
                }


                return devices;
            }
        }

        public List<DataAcessLayer.DeviceAccess.CustomersDetailsDevice> CustomerDetailsDevices()
        {
           
            List<DataAcessLayer.DeviceAccess.CustomersDetailsDevice> devices = new List<DataAcessLayer.DeviceAccess.CustomersDetailsDevice>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("CustomerDeviceDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;               
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.CustomersDetailsDevice device = new DataAcessLayer.DeviceAccess.CustomersDetailsDevice();
                    if (!(rdr["PID"] is DBNull))
                    {
                        device.PID = Convert.ToInt32(rdr["PID"]);
                    }
                    if (!(rdr["ID"] is DBNull))
                    {
                        device.ID = Convert.ToInt32(rdr["ID"]);
                    }
                    if (!(rdr["Part_ID"] is DBNull))
                    {
                        device.PartID = rdr["Part_ID"].ToString().Trim();
                    }
                    if (!(rdr["Serial_Number"] is DBNull))
                    {
                        device.SerialNumber = rdr["Serial_Number"].ToString().Trim();
                    }
                    if (!(rdr["Status"] is DBNull))
                    {
                        device.Status = rdr["Status"].ToString().Trim();
                    }
                    if (!(rdr["ParentName"] is DBNull))
                    {
                        device.ParentPartID = rdr["ParentName"].ToString();
                    }
                    if (!(rdr["Parent Serial Number"] is DBNull))
                    {
                        device.ParentSerialNumber = rdr["Parent Serial Number"].ToString().Trim();
                    }
                    if (!(rdr["CustID"] is DBNull))
                    {
                        device.CustID = Convert.ToInt32(rdr["CustID"]);
                    }
                    if (!(rdr["DeviceLocId"] is DBNull))
                    {
                        device.DeviceLocId = Convert.ToInt32(rdr["DeviceLocId"]);
                    }
                    devices.Add(device);
                }
                return devices;
            }
        }

        public List<DeviceAccess.CustomersDetailsPatient> CustomerDetailsPateints()
        {
           
            List<DataAcessLayer.DeviceAccess.CustomersDetailsPatient> devices = new List<DataAcessLayer.DeviceAccess.CustomersDetailsPatient>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("CustomerPatientsDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;              
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.CustomersDetailsPatient device = new DataAcessLayer.DeviceAccess.CustomersDetailsPatient();
                    device.ID = Convert.ToInt32(rdr["ID"]);
                    if (!(rdr["Customer_Id"] is DBNull))
                    {
                        device.Customer_Id = Convert.ToInt32(rdr["Customer_Id"]);
                    }
                    if (!(rdr["Patient_ID"] is DBNull))
                    {
                        device.PatientID = (rdr["Patient_ID"]).ToString().Trim();
                    }
                    if (!(rdr["IRFName"] is DBNull))
                    {
                        device.IRF = rdr["IRFName"].ToString().Trim();
                    }
                    if (!(rdr["Implant_Date"] is DBNull))
                    {
                        device.ImplantDate = rdr["Implant_Date"].ToString().Trim();
                    }
                    devices.Add(device);
                }

                return devices;
            }
        }

        public void SaveDevices(string status, string parentPartId, string parentSerialNumber, string customerId, string deviceId, string deviceLocationId, string updatedby)
        {
            bool isUpdated = false;
         
            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("UpdateDeviceDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@ParentPartId", parentPartId);
                cmd.Parameters.AddWithValue("@ParentSerialNumber", parentSerialNumber);
                cmd.Parameters.AddWithValue("@CustomerId", customerId);
                cmd.Parameters.AddWithValue("@DeviceId", deviceId);
                cmd.Parameters.AddWithValue("@DeviceLocationId", deviceLocationId);
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isUpdated", SqlDbType.Bit);
                cmd.Parameters["@isUpdated"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isUpdated = Convert.ToBoolean(cmd.Parameters["@isUpdated"].Value);
                }
                con.Close();
            }
        }

        public bool SaveCustomerDetails(string CustomerId, int AccountID, string CustomerName,string updatedby)
        {
            bool isSaved = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("SaveCustomerDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                cmd.Parameters.AddWithValue("@AccountID", AccountID);
                cmd.Parameters.AddWithValue("@CustomerName", CustomerName);               
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isSaved", SqlDbType.Bit);
                cmd.Parameters["@isSaved"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isSaved = Convert.ToBoolean(cmd.Parameters["@isSaved"].Value);
                }
                con.Close();
            }
            return isSaved;
        }

        public bool SaveDevicesDetails(string PartID, string SerialNumber,string status,int ParentID, string updatedby)
        {
            bool isSaved = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("SaveDevicesDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Part_ID", PartID);
                cmd.Parameters.AddWithValue("@Serial_Number", SerialNumber);
                cmd.Parameters.AddWithValue("@Status", status);
                cmd.Parameters.AddWithValue("@Parent_ID", ParentID);
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isSaved", SqlDbType.Bit);
                cmd.Parameters["@isSaved"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isSaved = Convert.ToBoolean(cmd.Parameters["@isSaved"].Value);
                }
                con.Close();
            }
            return isSaved;
        }

        public bool SaveDeviceLocationDetails(int DeviceID, int CustomerID, string SOID,string Current_Location,string updatedby)
        {
            bool isSaved = false;

            using (SqlConnection con = new SqlConnection(connectinstring))
            {

                SqlCommand cmd = new SqlCommand("SaveDeviceLocationDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@DeviceID", DeviceID);
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID);
                cmd.Parameters.AddWithValue("@SOID", SOID);
                cmd.Parameters.AddWithValue("@Current_Location", Current_Location);                
                cmd.Parameters.AddWithValue("@updatedby", updatedby);

                cmd.Parameters.Add("@isSaved", SqlDbType.Bit);
                cmd.Parameters["@isSaved"].Direction = ParameterDirection.Output;

                con.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    isSaved = Convert.ToBoolean(cmd.Parameters["@isSaved"].Value);
                }
                con.Close();
            }
            return isSaved;
        }        
                
        public List<DataAcessLayer.DeviceAccess.DeviceDetails> DeviceDetailDAL()
        {

            List<DataAcessLayer.DeviceAccess.DeviceDetails> devices = new List<DataAcessLayer.DeviceAccess.DeviceDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("DeviceDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.DeviceDetails device = new DataAcessLayer.DeviceAccess.DeviceDetails();
                    device.ID = Convert.ToInt32(rdr["ID"]);
                    device.PartID = rdr["Part_ID"].ToString().Trim();
                    device.SerialNumber = rdr["Serial_Number"].ToString().Trim();
                    device.Status = (rdr["Status"]).ToString();
                    if (!(rdr["ParentPartID"] is DBNull))
                    {
                        device.ParentPartID = (rdr["ParentPartID"]).ToString();
                    }
                    if (!(rdr["ParentSerialNumber"] is DBNull))
                    {
                        device.ParentSerialNumber = rdr["ParentSerialNumber"].ToString().Trim();
                    }
                    device.Updated = Convert.ToDateTime(rdr["Updated"]);
                    device.LastUpdatedBy = rdr["Last_Updated_By"].ToString().Trim();

                    devices.Add(device);
                }
                return devices;
            }
        }

        public List<DataAcessLayer.DeviceAccess.DeviceParentDetails> DeviceParentDetailsDAL()
        {

            
            List<DataAcessLayer.DeviceAccess.DeviceParentDetails> devices = new List<DataAcessLayer.DeviceAccess.DeviceParentDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("DeviceParentDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.DeviceParentDetails device = new DataAcessLayer.DeviceAccess.DeviceParentDetails();
                    device.ID = Convert.ToInt32(rdr["ID"]);
                    if (!(rdr["PID"] is DBNull))
                    { 
                        device.PID = Convert.ToInt32(rdr["PID"]); 
                    }
                    if (!(rdr["DeviceLocationId"] is DBNull))
                    {
                        device.DeviceLocationId = Convert.ToInt32(rdr["DeviceLocationId"]);
                    }
                   
                    device.PartID = rdr["Part_ID"].ToString().Trim();
                    device.SerialNumber = rdr["Serial_Number"].ToString().Trim();
                    if (!(rdr["ParentPartID"] is DBNull))
                    {
                        device.ParentPartID = (rdr["ParentPartID"]).ToString();
                    }
                    if (!(rdr["ParentSerialNumber"] is DBNull))
                    {
                        device.ParentSerialNumber = rdr["ParentSerialNumber"].ToString().Trim();
                    }

                    if (!(rdr["Customer_ID"] is DBNull))
                    {
                        device.Customer_ID =Convert.ToInt32( rdr["Customer_ID"].ToString().Trim());
                    }

                    device.Status = (rdr["Status"]).ToString();
                   
                    devices.Add(device);
                }
                return devices;
            }
        }

        public List<DataAcessLayer.DeviceAccess.DeviceLocationHistoryDetails> DeviceLocationHistoryDetailsDAL()
        {

            List<DataAcessLayer.DeviceAccess.DeviceLocationHistoryDetails> devices = new List<DataAcessLayer.DeviceAccess.DeviceLocationHistoryDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("DeviceLocationHistoryDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.DeviceLocationHistoryDetails device = new DataAcessLayer.DeviceAccess.DeviceLocationHistoryDetails();
                    device.DeviceID = Convert.ToInt32(rdr["Device_ID"]);
                    device.ID = Convert.ToInt32(rdr["ID"]);
                    device.CustomerName = (rdr["Customer_Name"]).ToString().Trim();
                    device.SalesOrderID = rdr["Sales Order ID"].ToString().Trim();
                    if (!(rdr["DeviceLocDate"] is DBNull))
                    {
                        device.DeviceLocDate = Convert.ToDateTime(rdr["DeviceLocDate"]);
                    }

                    devices.Add(device);
                }
                return devices;
            }
        }

        public List<DataAcessLayer.DeviceAccess.DeviceChildDetails> DeviceChildDetailsDAL()
        {
            List<DataAcessLayer.DeviceAccess.DeviceChildDetails> devices = new List<DataAcessLayer.DeviceAccess.DeviceChildDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("DeviceChildDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.DeviceChildDetails device = new DataAcessLayer.DeviceAccess.DeviceChildDetails();
                    device.ID = Convert.ToInt32(rdr["ID"]);
                    device.PID = Convert.ToInt32(rdr["PID"]);
                    device.PartID = (rdr["Part_ID"]).ToString();
                    device.SerialNumber = (rdr["Serial_Number"]).ToString().Trim();
                    device.Status = rdr["Status"].ToString().Trim();
                    device.ParentPartID = (rdr["ParentPartID"]).ToString();
                    if (!(rdr["ParentSerialNumber"] is DBNull))
                    {
                        device.ParentSerialNumber = rdr["ParentSerialNumber"].ToString().Trim();
                    }

                    if (!(rdr["ParentPartID"] is DBNull))
                    {
                        device.ParentPartID = (rdr["ParentPartID"]).ToString();
                    }                   

                    if (!(rdr["Customer_ID"] is DBNull))
                    {
                        device.Customer_ID = Convert.ToInt32(rdr["Customer_ID"].ToString().Trim());
                    }
                    if (!(rdr["DeviceLocationId"] is DBNull))
                    {
                        device.DeviceLocationId = Convert.ToInt32(rdr["DeviceLocationId"].ToString().Trim());
                    }    



                    device.Status = (rdr["Status"]).ToString();

                    devices.Add(device);
                }
                return devices;
            }
        }

        public List<DataAcessLayer.DeviceAccess.DeviceChildDetails> DeviceSubChildDetailsDAL(int id)
        {
            List<DataAcessLayer.DeviceAccess.DeviceChildDetails> devices = new List<DataAcessLayer.DeviceAccess.DeviceChildDetails>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("DeviceSubChildDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.DeviceChildDetails device = new DataAcessLayer.DeviceAccess.DeviceChildDetails();

                    device.ID = Convert.ToInt32(rdr["ID"]);
                    device.PID = Convert.ToInt32(rdr["PID"]);
                    device.PartID = (rdr["Part_ID"]).ToString();
                    device.SerialNumber = (rdr["Serial_Number"]).ToString().Trim();
                    device.Status = rdr["Status"].ToString().Trim();
                    device.ParentPartID = (rdr["ParentPartID"]).ToString().Trim();
                    if (!(rdr["ParentSerialNumber"] is DBNull))
                    {
                        device.ParentSerialNumber = rdr["ParentSerialNumber"].ToString().Trim();
                    }

                    if (!(rdr["ParentPartID"] is DBNull))
                    {
                        device.ParentPartID = (rdr["ParentPartID"]).ToString();
                    }

                    if (!(rdr["Customer_ID"] is DBNull))
                    {
                        device.Customer_ID = Convert.ToInt32(rdr["Customer_ID"].ToString().Trim());
                    }
                    if (!(rdr["DeviceLocationId"] is DBNull))
                    {
                        device.DeviceLocationId = Convert.ToInt32(rdr["DeviceLocationId"].ToString().Trim());
                    }



                    device.Status = (rdr["Status"]).ToString();

                    devices.Add(device);
                }
                return devices;
            }
        }

        public List<DataAcessLayer.SFAccounts>SF_AccountsDAL()
        {
            List<DataAcessLayer.SFAccounts> SF_AccountsList = new List<DataAcessLayer.SFAccounts>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("GetAllSF_Accounts", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.SFAccounts SF_Accounts = new DataAcessLayer.SFAccounts();
                    SF_Accounts.ID = Convert.ToInt32(rdr["ID"]);
                    SF_Accounts.Account_Name = (rdr["Account_Name"]).ToString().Trim();

                    SF_AccountsList.Add(SF_Accounts);
                }
                return SF_AccountsList;
            }
        }

        public List<DataAcessLayer.DeviceAccess.Customers> CustomersListDAL()
        {
            List<DataAcessLayer.DeviceAccess.Customers> CustomersList = new List<DataAcessLayer.DeviceAccess.Customers>();
            using (SqlConnection con = new SqlConnection(connectinstring))
            {
                SqlCommand cmd = new SqlCommand("GetAllCustomers", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DataAcessLayer.DeviceAccess.Customers Cust = new DataAcessLayer.DeviceAccess.Customers();
                    Cust.CustomerID = Convert.ToInt32(rdr["ID"]);
                    Cust.CustomerName = (rdr["Customer_Name"]).ToString().Trim();

                    CustomersList.Add(Cust);
                }
                return CustomersList;
            }
        }

        public DataTable GetAllSF_AccountsList()
        {
            DataTable dtProducts = new DataTable();
            using (SqlConnection conn = new SqlConnection(connectinstring))
            {
                using (SqlCommand sqlcmd = new SqlCommand())
                {
                    sqlcmd.CommandText = "GetAllSF_Accounts";
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Connection = conn;

                    conn.Open();
                    using (SqlDataReader rdr = sqlcmd.ExecuteReader())
                    {
                        dtProducts.Load(rdr);
                    }
                    conn.Close();
                }
            }
            return dtProducts;
        }

        public IEnumerable<DeviceAccess> ExpandablesStartLogHome
        {
            get
            {               

                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetExpandableStrtLogHome", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.LogId = Convert.ToInt32(rdr["id"]);
                        device.LogDescription = rdr["LogDescription"].ToString();
                        device.Source = rdr["source"].ToString();
                        device.StartDateTime = Convert.ToDateTime(rdr["starttime"]);
                        device.EndDateTime = Convert.ToDateTime(rdr["endtime"]);
                        device.LogStatus = Convert.ToString(rdr["message"]);
                        device.LogDate = Convert.ToDateTime(rdr["starttime"]);
                        device.LoggedBy = rdr["operator"].ToString();
                        devices.Add(device);
                    }

                }
                return devices;

            }

        }

        public IEnumerable<DeviceAccess> ExpandablesEndLogHome
        {
            get
            {
               List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetExpandableEndLogHome", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.LogId = Convert.ToInt32(rdr["id"]);
                        device.LogDescription = rdr["LogDescription"].ToString();
                        device.Source = rdr["source"].ToString();
                        device.StartDateTime = Convert.ToDateTime(rdr["starttime"]);
                        device.EndDateTime = Convert.ToDateTime(rdr["endtime"]);
                        device.LogStatus = Convert.ToString(rdr["message"]);
                        device.LogDate = Convert.ToDateTime(rdr["starttime"]);
                        device.LoggedBy = rdr["operator"].ToString();
                        devices.Add(device);
                    }

                }
                return devices;

            }

        }

        public IEnumerable<DeviceAccess> DeviceTrackingLog
        {
            get
            {              

                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetDeviceTrackingLog", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.LogId = Convert.ToInt32(rdr["LogId"]);
                        device.LogDescription = rdr["LogDescription"].ToString();
                        device.Source = rdr["Source"].ToString();
                        device.StartDateTime = Convert.ToDateTime(rdr["StartDateTime"]);
                        device.EndDateTime = Convert.ToDateTime(rdr["EndDateTime"]);
                        device.LogStatus = Convert.ToString(rdr["Status"]);
                        device.LogDate = Convert.ToDateTime(rdr["LogDate"]);
                        device.LoggedBy = rdr["LoggedBy"].ToString();
                        devices.Add(device);
                    }

                }
                return devices;

            }

        }

        public IEnumerable<DeviceAccess> ExpandablesDevicetrackingLog
        {
            get
            {
                List<DeviceAccess> devices = new List<DeviceAccess>();

                using (SqlConnection con = new SqlConnection(connectinstring))
                {
                    SqlCommand cmd = new SqlCommand("GetExpandableDataLogs", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        DeviceAccess device = new DeviceAccess();
                        device.LogId = Convert.ToInt32(rdr["id"]);
                        device.LogDescription = rdr["LogDescription"].ToString();
                        device.Source = rdr["source"].ToString();
                        device.StartDateTime = Convert.ToDateTime(rdr["starttime"]);
                        device.EndDateTime = Convert.ToDateTime(rdr["endtime"]);
                        device.LogStatus = Convert.ToString(rdr["message"]);
                        device.LogDate = Convert.ToDateTime(rdr["starttime"]);
                        device.LoggedBy = rdr["operator"].ToString();
                        devices.Add(device);
                    }

                }
                return devices;

            }

        }      


   
    }
}