﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAcessLayer
{
    [Table("Devices")]  

    public class DeviceAccess
    {
        public int ID { get; set; }
        public string Part_ID { get; set; }
        public string Serial_Number { get; set; }
        public string Status { get; set; }
        public List<string> StatusList { get; set; }
        public string Parent_ID { get; set; }
        public int PID { get; set; }

        public string Customer_Name { get; set; }
        public string ParentSerialNumber { get; set; }

        public int DeviceLocationId { get; set; }

        public bool Archived { get; set; }

        public bool IsDeleted { get; set; }

        public string ExpandableCustomerID { get; set; }
        public string ExpandableCustomerName { get; set; }
        public string SalesForceCustomerName { get; set; }

        //Customers
        public int SF_AccountID { get; set; }
        public string Account_ID { get; set; }
        public DateTime Updated { get; set; }
        public string Last_Altered_By { get; set; }
        //public string PatientCustomer_ID { get; set; }

        //Patients        
        public string PatientID { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public string  PatientsImplantDate { get; set; }
        public string PatientsImplantPhysician { get; set; }

        public string ImplantPhysician { get; set; }
        public string  PatientsDeathDate { get; set; }
        public int Customer_ID { get; set; }

        //DeviceTrackingLog
        public int LogId { get; set; }
        public string LogDescription { get; set; }
        public string Source { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string LogStatus { get; set; }
        public DateTime LogDate { get; set; }
        public string LoggedBy { get; set; }


        #region Patient Details Page

        public class PatientsDetails
        {
            public int ID { get; set; }
            public string Customer_Name { get; set; }
            [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
            public DateTime PatientsImplantDate { get; set; }
            [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yy}", ApplyFormatInEditMode = true)]
            public string Last_Altered_By { get; set; }
            public string Updated { get; set; }
            public string Patient_ID { get; set; }
            [DataType(DataType.Date)]
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime PatientsDeathDate { get; set; }
            public string PatientsImplantPhysician { get; set; }
            public int Customer_ID { get; set; }
            public string ParentPartID { get; set; }
        }
        public class PatientsAssignedDevices    //SFAccountsDetails
        {
            public int ID { get; set; }
            public string Part_ID { get; set; }
            public int PID { get; set; }
            public string Serial_Number { get; set; }
            public string Status { get; set; }
            public string ParentPartID { get; set; }
            public string Parent_Serial_Number { get; set; }
            public int Patient_ID { get; set; }
            public string Customer_ID { get; set; }

            public int DeviceLocID { get; set; }
        }

        public class ViewModelPateintDetails
        {
            public List<PatientsDetails> PatientsDetails { get; set; }
            public List<PatientsAssignedDevices> SFAccountsDetails { get; set; }
        }

        #endregion

        #region Customers Details Page

        public class CustomersDetails
        {
            public string CustomerID { get; set; }
            public string CustomerName { get; set; }
            public string SalesForceAccount { get; set; }
            public DateTime Updated { get; set; }
            public string LastAlteredBy { get; set; }
        }
        public class CustomersDetailsDevice
        {
            public int PID { get; set; }
            public int ID { get; set; }
            public string PartID { get; set; }
            public string SerialNumber { get; set; }
            public string Status { get; set; }
            public string ParentPartID { get; set; }
            public string ParentSerialNumber { get; set; }

            public int CustID { get; set; }
            public int DeviceLocId { get; set; }

            public string ParentID { get; set; }            
            

        }
        public class CustomersDetailsPatient
        {
            public int ID { get; set; }
            public string PatientID { get; set; }
            public int Customer_Id { get; set; }
            public string IRF { get; set; }
            public string ImplantDate { get; set; }
        }

        public class Customers
        {
            public int CustomerID { get; set; }
            public string CustomerName { get; set; }
          
        }

        public class ViewModelCustomerDetails
        {
            public List<CustomersDetails> customersDetails { get; set; }
            public List<CustomersDetailsDevice> customersDetailsDevice { get; set; }
            public List<CustomersDetailsPatient> customersDetailsPatient { get; set; }
        }

        #endregion

        #region Device Details Page

        public class DeviceDetails
        {
            public int ID { get; set; }
            public string PartID { get; set; }
            public string SerialNumber { get; set; }
            public string Status { get; set; }
            public string ParentPartID { get; set; }
            public string ParentSerialNumber { get; set; }
            public DateTime Updated { get; set; }
            public string LastUpdatedBy { get; set; }
            public string Serial_Number { get; set; }
            public int PID { get; set; } 
        }
        public class DeviceParentDetails
        {
            public int ID { get; set; }
            public int PID { get; set; }
            public int Customer_ID { get; set; }

            public int DeviceLocationId { get; set; }
            public string PartID { get; set; }

            public string Status { get; set; }

            public string Parent_ID { get; set; }
            public string SerialNumber { get; set; }
            public string ParentPartID { get; set; }
            public string ParentSerialNumber { get; set; }
        }
        public class DeviceLocationHistoryDetails
        {
            public int DeviceID { get; set; }
            public int ID { get; set; }
            public string CustomerName { get; set; }
            public string SalesOrderID { get; set; }
            public DateTime DeviceLocDate { get; set; }
        }
        public class DeviceChildDetails
        {
            public int ID { get; set; }
            public string PartID { get; set; }
            public string SerialNumber { get; set; }
            public string Status { get; set; }
            public string ParentPartID { get; set; }
            public string ParentSerialNumber { get; set; }
            public int PID { get; set; }
            public int Customer_ID { get; set; }
            public int DeviceLocationId { get; set; }
        }
        public class ViewModelDeviceDetails
        {
            public List<DeviceDetails> deviceDetails { get; set; }
            public List<DeviceParentDetails> deviceParentDetails { get; set; }
            public List<DeviceLocationHistoryDetails> deviceLocationHistoryDetails { get; set; }
            public List<DeviceChildDetails> deviceChildDetails { get; set; }
        }

        #endregion

    }

    [Table("SF_Accounts")]  
    public class SFAccounts
    {
        public int ID { get; set; }
        public string Account_Name { get; set; }
    }

    public class ViewModelCustomerDetailsNew
    {
        public List<DeviceAccess> deviceAccessdetais { get; set; }

        public List<DeviceAccess> expandablesStartDetails { get; set; }
        public List<DeviceAccess> expandablesEndDetails { get; set; }
        
    }

    public class ViewModelLogDetails
    {
        public List<DeviceAccess>SalesForceLogDetails { get; set; }

        public List<DeviceAccess> ExpandableDetails { get; set; }
        public List<DeviceAccess> RmDataLogDetails { get; set; }

    }

}
