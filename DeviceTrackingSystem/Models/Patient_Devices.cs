//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DeviceTrackingSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Patient_Devices
    {
        public int Patient_ID { get; set; }
        public int Device_ID { get; set; }
        public System.DateTime Start_Date { get; set; }
        public Nullable<System.DateTime> End_Date { get; set; }
        public System.DateTime Created { get; set; }
        public Nullable<System.DateTime> Updated { get; set; }
        public string Last_Updated_By { get; set; }
    }
}
