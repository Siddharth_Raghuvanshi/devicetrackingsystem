﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace DeviceTrackingSystem
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }
        protected void Session_End(object sender, EventArgs e)
        {
            if (Session["ID"] != null)
            {

                // return Redirect("~/Account/Login");

            }
            Session.Abandon();



        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 15;

        }

        //Custom Url
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                 "DeviceIndex",
                 "Device/",
                new { Controller = "Device", action = "DeviceIndex", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                 "CustomersIndex",
                 "Customers/",
                new { Controller = "Customers", action = "CustomersIndex", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                 "PatientsIndex",
                 "Patient/",
                new { Controller = "Patients", action = "PatientsIndex", id = UrlParameter.Optional }
                );
        }
    }
}