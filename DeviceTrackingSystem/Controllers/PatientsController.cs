﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAcessLayer;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.Security;
using DeviceTrackingSystem.Models;
using System.Net;
using WebMatrix.WebData;

namespace DeviceTrackingSystem.Controllers
{
    public class PatientsController : Controller
    {
        DeviceTrackingEntities db = new DeviceTrackingEntities();
        DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
        //
        // GET: /Patients/
        [HttpGet]
        [Authorize]
        public ActionResult PatientsIndex(string paging)
        {
            if (!String.IsNullOrEmpty(paging))
            {
                ViewBag.rowsPerPage = int.Parse(paging);
            }
            else
            {
                // default value
                ViewBag.rowsPerPage = 20;
            }
            DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
            List<DeviceAccess> lstPatients = deviceDataAcessLayer.PatientDevice.ToList();

            if (HttpContext.Request.QueryString.Count == 0 || HttpContext.Request.QueryString.Count > 0)
            {
                string showAll = string.Empty;
                if (HttpContext.Request.QueryString["ShowAll"] != null)
                    showAll = HttpContext.Request.QueryString["ShowAll"].ToString().Trim();
                if (showAll == "true")
                {
                    lstPatients = deviceDataAcessLayer.PatientDevice.ToList();
                }
                else
                {
                    lstPatients = deviceDataAcessLayer.PatientDevice.Where(p => p.PatientsDeathDate == "-").ToList();
                }
            }

            if (HttpContext.Request.QueryString.Count > 0)
            {
                string SearchKey = string.Empty;
                if (HttpContext.Request.QueryString["SearchKey"] != null)
                    SearchKey = HttpContext.Request.QueryString["SearchKey"].ToString().Trim();
                string ParentSearchKey = string.Empty;
                if (HttpContext.Request.QueryString["ParentSearchKey"] != null)
                    ParentSearchKey = HttpContext.Request.QueryString["ParentSearchKey"].ToString().Trim();
                string ParentKey = string.Empty;
                if (HttpContext.Request.QueryString["ParentKey"] != null)
                    ParentKey = HttpContext.Request.QueryString["ParentKey"].ToString().Trim();

                if (!string.IsNullOrEmpty(SearchKey) && string.IsNullOrEmpty(ParentSearchKey) && string.IsNullOrEmpty(ParentKey))
                {
                    lstPatients = lstPatients.Where(i => i.PatientID.Contains(SearchKey)).ToList();
                }
                else if (string.IsNullOrEmpty(SearchKey) && !string.IsNullOrEmpty(ParentSearchKey) && string.IsNullOrEmpty(ParentKey))
                {
                    lstPatients = lstPatients.Where(i => i.Customer_Name.Contains(ParentSearchKey)).ToList();
                }
                else if (string.IsNullOrEmpty(SearchKey) && string.IsNullOrEmpty(ParentSearchKey) && !string.IsNullOrEmpty(ParentKey))
                {
                    lstPatients = lstPatients.Where(i => i.PatientsImplantDate.ToString().Contains(ParentKey)).ToList();
                }
                else if (!string.IsNullOrEmpty(SearchKey) && !string.IsNullOrEmpty(ParentSearchKey) && string.IsNullOrEmpty(ParentKey))
                {
                    lstPatients = lstPatients.Where(i => i.PatientID.Contains(SearchKey) && i.Customer_Name.Contains(ParentSearchKey)).ToList();
                }
                else if (!string.IsNullOrEmpty(SearchKey) && !string.IsNullOrEmpty(ParentSearchKey) && !string.IsNullOrEmpty(ParentKey))
                {
                    lstPatients = lstPatients.Where(i => i.PatientID.Contains(SearchKey) && i.Customer_Name.Contains(SearchKey) && i.PatientsImplantDate.ToString().Contains(ParentKey)).ToList();
                }
            }
            return View(lstPatients);
        }

        //
        // GET: /Patients/Details/5
        //[Authorize]
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        public ActionResult Details(int id = 0, string CustId = "")
        {
            DataAcessLayer.DeviceAccess.ViewModelPateintDetails PateintsModel = new DataAcessLayer.DeviceAccess.ViewModelPateintDetails();
            PateintsModel.PatientsDetails = deviceLayer.ParentPatientDetail().Where(p => p.ID == id).ToList();
            PateintsModel.SFAccountsDetails = deviceLayer.ChildPatientDetail().Where(p => p.Patient_ID == id && p.Customer_ID == CustId).ToList();
            return View(PateintsModel);
        }

        //
        // GET: /Patients/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Patients/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Patients/Edit/5
        [Authorize(Roles = "ReadWrite")]
        public ActionResult Edit(int id = 0, int CustId = 0)
        {
            Patient patient = new Patient();
            DeviceAccess deviceAccess = new DeviceAccess();
         
            ViewBag.id = id;
            ViewBag.CustId = CustId;
            try
            {
                patient = db.Patients.Find(id);
                if (patient == null)
                {
                    return HttpNotFound();
                }                          
               
                ViewBag.customerName = new SelectList(db.Customers, "ID", "Customer_Name", CustId);
                


            }
            catch (Exception ex)
            {

            }

            return View(patient);
        }

        //
        // POST: /Patients/Edit/5
        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                string ImplantDate = Request.Form["Implant_Date"];
                string ImplantPhyscian = Request.Form["Implant_Physcian"];
                string ExplantDate = Request.Form["Explant_Date"];
                int CustomerID = Convert.ToInt32(Request.Form["Customer_ID"]);
                var updatedby = User.Identity.Name;


                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SavePatients(CustomerID, ImplantDate, ImplantPhyscian, ExplantDate, id, updatedby);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        return RedirectToAction("PatientsIndex");
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("PatientsIndex");
                    }

                }
                return RedirectToAction("PatientsIndex");

            }
            catch
            {
                return View();
            }
        }
        [Authorize]
        //
        // GET: /Patients/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        [Authorize(Roles = "ReadWrite")]
        public ActionResult DeletePatientRecord(int id)
        {
            if (ModelState.IsValid)
            {
                DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                var updatedby = User.Identity.Name;
                bool IsDeleted = deviceLayer.DeletePatientRecord(id, updatedby);
                if (IsDeleted)
                {
                    TempData["message"] = "Record Successfully Deleted";
                }
                return RedirectToAction("PatientsIndex");
            }
            return View();
        }
        //
        // POST: /Patients/Delete/5      
        [HttpPost]
        [Authorize(Roles = "ReadWrite")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [Authorize(Roles = "ReadWrite")]
        public ActionResult PatientDetailsEdit(int id = 0, string CustId = "", int DeviceId = 0, int patientId = 0)
        {
            List<DataAcessLayer.DeviceAccess.PatientsAssignedDevices> objsFAccountsDetails = new List<DataAcessLayer.DeviceAccess.PatientsAssignedDevices>();
            ViewBag.id = id;
            try
            {
                objsFAccountsDetails = deviceLayer.ChildPatientDetail();
                ViewBag.partId = new SelectList(deviceLayer.ChildPatientDetail(), "ID", "Part_ID", id);
                ViewBag.serialNumber = new SelectList(deviceLayer.ChildPatientDetail(), "ID", "Serial_Number", id);
                ViewBag.status = new SelectList(deviceLayer.ChildPatientDetail(), "ID", "Status", id);
                ViewBag.parentPartID = new SelectList(deviceLayer.ChildPatientDetail(), "ID", "ParentPartID", id);
                ViewBag.parentSerialNumber = new SelectList(deviceLayer.ChildPatientDetail(), "ID", "Parent_Serial_Number", id);
                ViewBag.custID = CustId.ToString();
                // ViewBag.ID = id;
                ViewBag.id = id;
                ViewBag.patientId = patientId;
            }

            catch (Exception ex)
            {

            }
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "ReadWrite")]
        public ActionResult PatientDetailsEdit(int id, FormCollection collection, int deviceId = 0, int patientId = 0, string CustId = "")
        {
            try
            {
                // TODO: Add update logic here
                string PID = Request.Form["ID"];
                string newpID = PID.Split(',')[0];
                int NewPartId = Convert.ToInt32(newpID);
                int DeviceId = Convert.ToInt32(deviceId);
                int PatientId = Convert.ToInt32(patientId);

                var updatedby = User.Identity.Name;


                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SavePatientChildDetails(NewPartId, DeviceId, PatientId, updatedby);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        return RedirectToAction("Details", new { id = PatientId, CustId = CustId });
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("Details", new { id = PatientId, CustId = CustId });
                    }

                }
                return RedirectToAction("Details", new { id = PatientId, CustId = CustId });

            }
            catch
            {
                return View();
            }
        }
         [Authorize(Roles = "ReadWrite")]
        public ActionResult PatientSubDetails(int id = 0, string serialNumber = "", string status = "", string parentPartID = "", string parentSerialNumber = "", string CustId = "")
        {
            ViewBag.id = id;
            ViewBag.serialNumber = serialNumber.ToString();
            ViewBag.status = status.ToString();
            ViewBag.parentPartID = parentPartID.ToString();
            ViewBag.parentSerialNumber = parentSerialNumber.ToString();
            ViewBag.custID = CustId.ToString();

            return View();
        }
    }
}
