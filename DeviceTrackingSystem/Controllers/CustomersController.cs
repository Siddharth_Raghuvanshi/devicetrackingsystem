﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAcessLayer;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.Security;
using DeviceTrackingSystem.Models;
using System.Net;

namespace DeviceTrackingSystem.Controllers
{
    public class CustomersController : Controller
    {
        DeviceTrackingEntities db = new DeviceTrackingEntities();
        DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
        [HttpGet]
        // GET: /Customers/
        [Authorize]
        public ActionResult CustomersIndex(string paging)
        {
            if (!String.IsNullOrEmpty(paging))
            {
                ViewBag.rowsPerPage = int.Parse(paging);
            }
            else
            {
                // default value
                ViewBag.rowsPerPage = 20;
            }
            DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
            List<DeviceAccess> lstLog = deviceDataAcessLayer.CustomerDevice.ToList();
            if (HttpContext.Request.QueryString.Count == 0 || HttpContext.Request.QueryString.Count > 0)
            {
                string showAll = string.Empty;
                if (HttpContext.Request.QueryString["ShowAll"] != null)
                    showAll = HttpContext.Request.QueryString["ShowAll"].ToString().Trim();
                if (showAll == "true")
                {
                    lstLog = deviceDataAcessLayer.CustomerDevice.ToList();
                }
                else if(showAll == "false")
                {
                    lstLog = deviceDataAcessLayer.CustomerDevice.Where(c => c.Archived == true).ToList();
                }
                else
                {
                    lstLog = deviceDataAcessLayer.CustomerDevice.Where(c => c.Archived == false).ToList();
                }
            }
            if (HttpContext.Request.QueryString.Count > 0)
            {
                string SearchKey = string.Empty;
                if (HttpContext.Request.QueryString["SearchKey"] != null)
                    SearchKey = HttpContext.Request.QueryString["SearchKey"].ToString().Trim();
                if (!string.IsNullOrEmpty(SearchKey))
                {
                    lstLog = lstLog.Where(i => i.ExpandableCustomerID.Contains(SearchKey) || i.ExpandableCustomerName.Contains(SearchKey)).ToList();
                }
                
            }

            return View(lstLog);
        }

        //
        // GET: /Customers/Details/5

        //
        // GET: /Customers/Details/5
        [Authorize(Roles = "ReadWrite")]
        public ActionResult Details(int id = 0, int Pid = 0)
        {
            DataAcessLayer.DeviceAccess.ViewModelCustomerDetails objviewModelCustomerDetails = new DataAcessLayer.DeviceAccess.ViewModelCustomerDetails();
            objviewModelCustomerDetails.customersDetails = deviceDataAcessLayer.CustomerDetails(id).ToList();
            objviewModelCustomerDetails.customersDetailsDevice = deviceDataAcessLayer.CustomerDetailsDevices().Where(c => c.ID == id || c.PID == Pid).ToList();
            objviewModelCustomerDetails.customersDetailsPatient = deviceDataAcessLayer.CustomerDetailsPateints().Where(c => c.Customer_Id == id).ToList();


            return View(objviewModelCustomerDetails);
        }

        //
        // GET: /Customers/Create
        [Authorize(Roles = "ReadWrite")]
        public ActionResult Create()
        {

           // Class1 dbconnection = new Class1();
            DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
            List<DataAcessLayer.SFAccounts> pcontent = new List<DataAcessLayer.SFAccounts>();
            {
                //Get all page content from TblHome table
                pcontent = deviceDataAcessLayer.SF_AccountsDAL();

            };
            List<SelectListItem> accountList = new List<SelectListItem>();
            //List<string> items = new List<string>();
            foreach (var item in pcontent)
            {
                accountList.Add(new SelectListItem
                {
                    Text = item.Account_Name,
                    Value = item.ID.ToString()
                });
            }

            ViewBag.AccountNameList = accountList;
            return View();         
        }

        //
        // POST: /Customers/Create

        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                
                string Customer_ID = Request.Form["Customer_ID"];
                int AccountID =Convert.ToInt32(Request.Form["ID"]);
                string Customer_Name = Request.Form["Customer_Name"];

                string updatedby = User.Identity.Name;
                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isSaved = deviceLayer.SaveCustomerDetails(Customer_ID, AccountID, Customer_Name, updatedby);
                    if (isSaved)
                    {
                        TempData["message"] = "Record Successfully Saved";
                        //return RedirectToAction("CustomersIndex");
                    }
                    else
                    {
                        TempData["message"] = "Record Not Saved";
                       // return RedirectToAction("CustomersIndex");
                    }

                }
               return RedirectToAction("Create");

            }
            catch
            {
                return View();
            }
        }

       
        

        //
        // POST: /Customers/Edit/5
        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection, int CustId = 0)
        {
            try
            {
                // TODO: Add update logic here

                string ID = Request.Form["ID"];
                var newID = ID.Split(',')[1];
                int AccountId = Convert.ToInt32(newID);
                int CustomerID = Convert.ToInt32(CustId);
                var updatedby = User.Identity.Name;


                string archived = Request.Form["templateId"];
                int a = archived.IndexOf(",");
                string archivednew = (a >= 0) ? archived.Substring(0, a) : archived;

                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SaveCustomers(AccountId, CustomerID, updatedby, archivednew);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        return RedirectToAction("CustomersIndex");
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("CustomersIndex");
                    }

                }
                return RedirectToAction("CustomersIndex");

            }
            catch
            {
                return View();
            }
        }
        [Authorize(Roles = "ReadWrite")]
        public ActionResult Edit(int id = 0, string Customer_Name = "", string customerCode = "", int CustId = 0)
        {
            SF_Accounts sfaccounts = new SF_Accounts();
          
            DeviceAccess device = new DeviceAccess();
            DeviceDataAcessLayer customer = new DeviceDataAcessLayer();
            if (customer == null)
            {
                return HttpNotFound();
            }
            sfaccounts = db.SF_Accounts.Find(id);
            ViewBag.sfAccountName = new SelectList(db.SF_Accounts.OrderBy(y => y.Account_Name), "ID", "Account_Name", id);
            bool IsArchived = customer.GetIsArchived(CustId, id);
            ViewBag.Isarchived = IsArchived;
            TempData["IsArchived"] = IsArchived;
            TempData["customerName"] = Customer_Name;
            TempData["customerID"] = customerCode;
            return View(sfaccounts);
        }
        //
        // GET: /Customers/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            return View();
        }
        [Authorize(Roles = "ReadWrite")]
        public ActionResult CustomersDelete(string id = "")
        {
            if (ModelState.IsValid)
            {
                DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                var updatedby = User.Identity.Name;
                bool IsDeleted = deviceLayer.DeleteCustomerRecord(id, updatedby);
                if (IsDeleted)
                {
                    TempData["message"] = "Record Successfully Deleted";
                }
                return RedirectToAction("CustomersIndex");
            }
            return View();
        }
        //
        // POST: /Customers/Delete/5
        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [Authorize(Roles = "ReadWrite")]
        public ActionResult CustomerDetailsDeviceDetails(int id = 0, string partID = "", string serialNumber = "", string status = "", string parentPartID = "", string parentSerialNumber = "")
        {

            ViewBag.id = id;
            ViewBag.partID = partID.ToString();
            ViewBag.serialNumber = serialNumber.ToString();
            ViewBag.status = status.ToString();
            ViewBag.parentPartID = parentPartID.ToString();
            ViewBag.parentSerialNumber = parentSerialNumber.ToString();
            return View();
        }
        [Authorize(Roles = "ReadWrite")]
        public ActionResult CustomerDetailsDeviceEdit(int id = 0, int Pid = 0)
        {
            //if(id)

            List<DataAcessLayer.DeviceAccess.CustomersDetailsDevice> objcustomersDetailsDevice = new List<DataAcessLayer.DeviceAccess.CustomersDetailsDevice>();
            ViewBag.id = id;
            try
            {
                objcustomersDetailsDevice = deviceDataAcessLayer.CustomerDetailsDevices();
                ViewBag.partID = new SelectList(deviceDataAcessLayer.CustomerDetailsDevices(), "PID", "PartID", Pid);
                ViewBag.serialNumber = new SelectList(deviceDataAcessLayer.CustomerDetailsDevices(), "PID", "SerialNumber", Pid);
                ViewBag.status = new SelectList(deviceDataAcessLayer.CustomerDetailsDevices(), "PID", "Status", Pid);
                ViewBag.parentPartID = new SelectList(deviceDataAcessLayer.CustomerDetailsDevices(), "PID", "ParentPartID", Pid);
                ViewBag.parentSerialNumber = new SelectList(deviceDataAcessLayer.CustomerDetailsDevices(), "PID", "ParentSerialNumber", Pid);
                ViewBag.id = id;
            }

            catch (Exception ex)
            {

            }
            return View();
        }

        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
        public ActionResult CustomerDetailsDeviceEdit(int id, FormCollection collection, int CustId = 0, int Pid = 0)
        {
            try
            {
                // TODO: Add update logic here

                string partID = Request.Form["PID"];
                var newpartID = partID.Split(',')[1];
                int NewPartID = Convert.ToInt32(newpartID);
                int DeviceID = Convert.ToInt32(Pid);
                int CustomerID = Convert.ToInt32(CustId);
                var updatedby = User.Identity.Name;
                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SaveCustomersDevices(NewPartID, CustomerID, DeviceID, updatedby);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        // return RedirectToAction("CustomersDetails",6);
                        return RedirectToAction("Details", new { id = CustomerID });
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("Details", new { id = CustomerID });
                    }

                }
                return RedirectToAction("Details", new { id = CustomerID });

            }
            catch
            {
                return View();
            }
        }
        [Authorize(Roles = "ReadWrite")]
        public ActionResult CustomerDetailsPatientDetails(int id = 0, string patientID = "", string iRF = "", string implantDate = "", int customerID = 0)
        {
            ViewBag.id = id;
            ViewBag.customerID = customerID;
            ViewBag.patientID = patientID.ToString();
            ViewBag.iRF = iRF.ToString();
            ViewBag.implantDate = implantDate.ToString();
            return View();
        }
        [Authorize(Roles = "ReadWrite")]

        [HttpGet]
        public ActionResult CustomerDetailsPatientEdit(int id = 0, int customerID = 0, int CustId = 0)
        {
            List<DataAcessLayer.DeviceAccess.CustomersDetailsPatient> objcustomersDetailsDevice = new List<DataAcessLayer.DeviceAccess.CustomersDetailsPatient>();
            ViewBag.customerID = customerID;
            ViewBag.id = id;
            try
            {
                objcustomersDetailsDevice = deviceDataAcessLayer.CustomerDetailsPateints();
                ViewBag.patientID = new SelectList(deviceDataAcessLayer.ParentPatientDetail(), "ID", "Patient_ID", id);
                ViewBag.iRF = new SelectList(deviceDataAcessLayer.ParentPatientDetail(), "ID", "PatientsImplantPhysician", id);
                ViewBag.implantDate = new SelectList(deviceDataAcessLayer.ParentPatientDetail(), "ID", "PatientsImplantDate", id);
            }

            catch (Exception ex)
            {

            }
            return View();
        }
        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
        public ActionResult CustomerDetailsPatientEdit(int id, FormCollection collection, int customerID = 0, int patientID = 0)
        {
            try
            {
                // TODO: Add update logic here
                string PID = Request.Form["ID"];
                var newpID = PID.Split(',')[1];
                int NewPatientID = Convert.ToInt32(newpID);
                int CustomerID = Convert.ToInt32(customerID);
                int PatientID = Convert.ToInt32(id);
                var updatedby = User.Identity.Name;
                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SaveCustomerPatients(NewPatientID, CustomerID, PatientID, updatedby);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        // return RedirectToAction("CustomersDetails",6);
                        return RedirectToAction("Details", new { id = CustomerID });
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("Details", new { id = CustomerID });
                    }
                }
                return RedirectToAction("Details", new { id = CustomerID });

            }
            catch
            {
                return View();
            }
        }
    }
    
}
