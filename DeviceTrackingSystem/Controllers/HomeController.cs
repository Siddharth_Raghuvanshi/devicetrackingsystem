﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DataAcessLayer;
using Microsoft.VisualBasic.FileIO;

//tesing Souce tree TESTING
namespace DeviceTrackingSystem.Controllers
{
    public class HomeController : Controller
    {
          [Authorize]
        public ActionResult Index()
        {
           
                // default value
                ViewBag.rowsPerPage = 20;
            
           // ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
            List<DeviceAccess> lstDeviceTrackingLog = deviceDataAcessLayer.DeviceTrackingLogHome.ToList();
     
            ViewModelCustomerDetailsNew viewModelCustomerDetailsNew = new ViewModelCustomerDetailsNew();
          
            viewModelCustomerDetailsNew.deviceAccessdetais = deviceDataAcessLayer.DeviceTrackingLogHome.ToList();
            viewModelCustomerDetailsNew.expandablesStartDetails = deviceDataAcessLayer.ExpandablesStartLogHome.ToList();
            viewModelCustomerDetailsNew.expandablesEndDetails = deviceDataAcessLayer.ExpandablesEndLogHome.ToList();
            return View(viewModelCustomerDetailsNew);
           
        }
          [HttpPost]
          public ActionResult ViewLogs(HttpPostedFileBase file)
          {
              ViewBag.rowsPerPage =20;
              int count = 0;
              Int64 NoofRecordsDeleted = 0;
              DateTime StrtDate = DateTime.UtcNow;          
               DateTime EndDate = DateTime.UtcNow;
               string StartDate = Convert.ToString(StrtDate);
              // Set up DataTable place holder
              DataTable dt = new DataTable();
              if (ModelState.IsValid)
              {
                 
                  if (file == null)
                  {
                      ModelState.AddModelError("File", "Please Upload Your file");
                      TempData["message"] = "Please Upload Your file";
                  }
                  else if (file.ContentLength > 0)
                  {
                      int MaxContentLength = 1024 * 1024 * 3; //3 MB
                      string[] AllowedFileExtensions = new string[] {".csv"};

                      if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                      {
                          ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                          TempData["message"] = "Please file of type: " + string.Join(", ", AllowedFileExtensions);
                      }

                      else if (file.ContentLength > MaxContentLength)
                      {
                          ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                          TempData["message"] = "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB";
                      }
                      else
                      {
                          //TO:DO
                          try 
                          {
                              
                              var fileName = Path.GetFileName(file.FileName);
                              var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                              file.SaveAs(path);
                              //Process the CSV file and capture the results to our DataTable place holder
                              //dt = ProcessCSV(path);
                              dt = GetDataTabletFromCSVFile(file.InputStream);
                              ////Process the DataTable and capture the results to our SQL Bulk copy
                              //ViewData["Feedback"] = ProcessBulkCopy(dt);

                              DataTable dtCols = new DataTable();
                              string tblName = "tblRMData";
                            
                              if (dt.Rows.Count == 0)
                              {
                                  TempData["message"] = "No Records Found.";
                                  //lblStatus.Text = " No Records Found.";
                                  
                              }
                              else
                              {
                                  using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString))
                                  {
                                      con.Open();
                                      string query = "SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" + tblName + "' AND ORDINAL_POSITION<>1";
                                      SqlDataAdapter da = new SqlDataAdapter(query, con);
                                      da.Fill(dtCols);
                                  }
                                  string colNames = "";
                                  for (int i = 0; i < dtCols.Rows.Count; i++)
                                  {
                                      bool colFound = false;
                                      for (int j =2; j < dt.Columns.Count; j++)
                                      {
                                          if (dt.Columns[j].ColumnName.ToString().ToLower().Trim().Replace("#", ".")
                                              == dtCols.Rows[i]["COLUMN_NAME"].ToString().ToLower().Trim().Replace("#", "."))
                                          {
                                              colFound = true;
                                              break;
                                          }
                                      }
                                      if (!colFound)
                                          colNames += dtCols.Rows[i]["COLUMN_NAME"].ToString() + ",";
                                  }
                                  if (colNames != "" && colNames.ToLower().Trim() != "Patient ID,".ToLower().Trim())
                                  {
                                      TempData["message"] = "Please select a valid File. Column Names Missing: " + colNames.Substring(0, colNames.Length - 1);
                                    
                                  }

                                  using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString))
                                  {
                                      con.Open();
                                      string query = "insert into " + tblName + "(";
                                      for (int i = 0; i < dt.Columns.Count; i++)
                                      {
                                          query += "[" + dt.Columns[i].ColumnName + "],";

                                      }

                                      query = query.Substring(0, query.Length - 1) + ")";

                                      for (int j = 0; j < dt.Rows.Count; j++)
                                      {
                                          bool isEmpty = true;
                                          for (int i = 0; i < dt.Columns.Count; i++)
                                          {
                                              if (dt.Rows[j][i].ToString().Trim() != "")
                                              {
                                                  isEmpty = false;
                                              }
                                          }
                                          if (isEmpty)
                                          {
                                              //emptyRows++;
                                              continue;
                                          }
                                          string query2 = query;
                                          query2 += " select ";
                                          for (int i = 0; i < dt.Columns.Count; i++)
                                          {
                                              if (checkDatatype(dt.Columns[i].ColumnName, dtCols))
                                              {
                                                 DateTime date= Convert.ToDateTime(dt.Rows[j][i].ToString(), System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat );

                                                 query2 += (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + Convert.ToDateTime(date) + "'") + " as [" + dt.Columns[i].ColumnName + "],";
                                           
                                              // query2 += (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + Convert.ToDateTime(dt.Rows[j][i].ToString()) + "'") + " as [" + dt.Columns[i].ColumnName + "],";
                                              }
                                              else
                                              {
                                                  query2 += (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + dt.Rows[j][i].ToString().Replace("'", "''") + "'") + " as [" + dt.Columns[i].ColumnName + "],";
                                              }
                                          }
                                          
                                          query2 = query2.Substring(0, query2.Length - 1);

                                          SqlCommand cmd1 = new SqlCommand(query2, con);
                                          cmd1.ExecuteNonQuery();
                                          count++;
                                         
                                      }
                                  }

                                  string msg =  count.ToString() + " Records Imported";
                                  TempData["message"] = msg;
                                  NoofRecordsDeleted = DeleteDuplicateRMData();
                                  if (NoofRecordsDeleted > 0)
                                  {
                                      //msg = (dt.Rows.Count - NoofRecordsDeleted).ToString() + " Records Imported, " + NoofRecordsDeleted.ToString() + " Duplicate Records Deleted";
                                      if (count == NoofRecordsDeleted)
                                      {
                                          msg = (NoofRecordsDeleted).ToString() + " Records Imported, " + NoofRecordsDeleted.ToString() + " Duplicate Records Deleted";
                                      }
                                      else
                                      {
                                          msg = (count - NoofRecordsDeleted).ToString() + " Records Imported, " + NoofRecordsDeleted.ToString() + " Duplicate Records Deleted";
                                      }
                                     
                                      TempData["message"] = msg;
                                  }
                                  EndDate = DateTime.UtcNow;
                                  string EndDt = Convert.ToString(EndDate);
                                  using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString))
                                  {
                                      con.Open();
                                      var loggedby = User.Identity.Name;

                                      string Logquery = "Insert into DeviceTrackingLog(LogDescription,Source,StartDateTime,EndDateTime,Status,LogDate,LoggedBy) Values('" + msg.Trim() + "','RM Data','" + StartDate + "','" + EndDt + "','" + msg.Trim() + "','" + DateTime.UtcNow + "','" + loggedby + "')";
                                      SqlCommand cmd2 = new SqlCommand(Logquery, con);
                                      cmd2.ExecuteNonQuery();
                                  }
                              }

                              ModelState.Clear();
                              TempData["message"] = "File uploaded successfully";
                            
                          
                          }
                          catch (Exception ex)
                          {                            
                              TempData["message"] = ex.Message;
                          }
                          
                      }
                  }
              }
              DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
              List<DeviceAccess> lstDeviceTrackingLog = deviceDataAcessLayer.DeviceTrackingLog.ToList();
              //Tidy up
              dt.Dispose();
              return View(lstDeviceTrackingLog);
              #region
              /////////////////////////////////////////New Code///////////////////////////////////

              //int count = 0;
              //int updateCount = 0;
              //string filePath = string.Empty;
              //string fileType = string.Empty;
              //Int64 NoofRecordsDeleted = 0;
              ////int dupCount = 0, emptyRows = 0;

              //try
              //{
              //    if (uploadDoc.HasFile)
              //    {
              //        if (!uploadDoc.FileName.ToLower().EndsWith(".csv"))
              //        {
              //            lblStatus.Text = "Please Select Valid \".csv\" File";
              //            lblStatus.ForeColor = System.Drawing.Color.Red;
              //            return;
              //        }
              //        if (ddlData.SelectedIndex < 0)
              //        {
              //            lblStatus.Text = "Please Select Patient Data Type";
              //            lblStatus.ForeColor = System.Drawing.Color.Red;
              //            return;
              //        }
              //        if (ddlData.SelectedIndex == 1 && rtbPatientID.Text.Trim() == "")
              //        {
              //            lblStatus.Text = "Please Enter Patient ID";
              //            lblStatus.ForeColor = System.Drawing.Color.Red;
              //            return;
              //        }

              //        //Save File
              //        if (uploadDoc.HasFile)
              //            SaveFile(uploadDoc.PostedFile);

              //        DataTable dt = GetDataTabletFromCSVFile(uploadDoc.FileContent);
              //        DataTable dtCols = new DataTable();
              //        string tblName = string.Empty;
              //        if (dt.Rows.Count == 0)
              //        {
              //            lblStatus.Text = " No Records Found.";
              //            lblStatus.ForeColor = System.Drawing.Color.BlueViolet;
              //            return;
              //        }
              //        else
              //        {
              //            tblName = tables[ddlData.SelectedIndex];
              //            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString))
              //            {
              //                con.Open();
              //                string query = "SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" + tblName + "' AND ORDINAL_POSITION<>1";
              //                SqlDataAdapter da = new SqlDataAdapter(query, con);
              //                da.Fill(dtCols);
              //            }
              //            string colNames = "";
              //            for (int i = 0; i < dtCols.Rows.Count; i++)
              //            {
              //                bool colFound = false;
              //                for (int j = 0; j < dt.Columns.Count; j++)
              //                {
              //                    if (dt.Columns[j].ColumnName.ToString().ToLower().Trim().Replace("#", ".")
              //                        == dtCols.Rows[i]["COLUMN_NAME"].ToString().ToLower().Trim().Replace("#", "."))
              //                    {
              //                        colFound = true;
              //                        break;
              //                    }
              //                }
              //                if (!colFound)
              //                    colNames += dtCols.Rows[i]["COLUMN_NAME"].ToString() + ",";
              //            }
              //            if (colNames != "" && colNames.ToLower().Trim() != "Patient ID,".ToLower().Trim())
              //            {
              //                lblStatus.Text = "Please select a valid File. Column Names Missing: " + colNames.Substring(0, colNames.Length - 1);
              //                lblStatus.ForeColor = System.Drawing.Color.Red;
              //                return;
              //            }
              //        }

              //        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString))
              //        {
              //            con.Open();
              //            string query = "insert into " + tblName + "(";
              //            for (int i = 0; i < dt.Columns.Count; i++)
              //            {
              //                query += "[" + dt.Columns[i].ColumnName + "],";

              //            }

              //            query = query.Substring(0, query.Length - 1) + ")";

              //            for (int j = 0; j < dt.Rows.Count; j++)
              //            {
              //                bool isEmpty = true;
              //                for (int i = 0; i < dt.Columns.Count; i++)
              //                {
              //                    if (dt.Rows[j][i].ToString().Trim() != "")
              //                    {
              //                        isEmpty = false;
              //                    }
              //                }
              //                if (isEmpty)
              //                {
              //                    //emptyRows++;
              //                    continue;
              //                }

              //                #region Check duplicate record before insertion

              //                //string dupQuery = "Select Count(1) from " + tblName + " where ";
              //                //for (int i = 0; i < dt.Columns.Count; i++)
              //                //{
              //                //    if (dt.Columns[i].ColumnName.ToString() == "Patient ID" || dt.Columns[i].ColumnName.ToString() == "When"
              //                //            || dt.Columns[i].ColumnName.ToString() == "Product Experience Report: ID"
              //                //            || dt.Columns[i].ColumnName.ToString() == "Product Experience Report: Product Experience Report Name"
              //                //            || dt.Columns[i].ColumnName.ToString() == "Implant Traceability Form: ID"
              //                //            || dt.Columns[i].ColumnName.ToString() == "Implant Traceability Form: Implant Traceability Form")
              //                //    {
              //                //        if (checkDatatype(dt.Columns[i].ColumnName, dtCols))
              //                //        {
              //                //            if (dt.Rows[j][i].ToString().Trim() == "")
              //                //            {
              //                //                dupQuery += "[" + dt.Columns[i].ColumnName + "] " + (dt.Rows[j][i].ToString().Trim() == "" ? "IS NULL" : "'" + convertDate(dt.Rows[j][i].ToString()) + "'") + " AND ";
              //                //            }
              //                //            else
              //                //            {
              //                //                dupQuery += "[" + dt.Columns[i].ColumnName + "]=" + (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + convertDate(dt.Rows[j][i].ToString()) + "'") + " AND ";
              //                //            }

              //                //        }
              //                //        else
              //                //        {

              //                //            if (dt.Rows[j][i].ToString().Trim() == "")
              //                //            {
              //                //                dupQuery += "[" + dt.Columns[i].ColumnName + "] " + (dt.Rows[j][i].ToString().Trim() == "" ? "IS NULL" : "'" + dt.Rows[j][i].ToString().Replace("'", "''") + "'") + " AND ";
              //                //                //dupQuery += "[" + dt.Columns[i].ColumnName + "]=" + (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + dt.Rows[j][i].ToString().Replace("'", "''") + "'") + " AND ";
              //                //            }
              //                //            else
              //                //            {
              //                //                dupQuery += "[" + dt.Columns[i].ColumnName + "]=" + (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + dt.Rows[j][i].ToString().Replace("'", "''") + "'") + " AND ";
              //                //            }
              //                //        }
              //                //    }
              //                //}

              //                //dupQuery = dupQuery.Substring(0, dupQuery.Length - 4);
              //                //SqlCommand cmd = new SqlCommand(dupQuery, con);
              //                //if (Convert.ToInt32(cmd.ExecuteScalar().ToString()) > 0)
              //                //{
              //                //    dupCount++;
              //                //    continue;
              //                //}

              //                #endregion

              //                #region Update Patient information

              //                if (ddlData.SelectedIndex == 0)
              //                {
              //                    string whereCondition = string.Empty;

              //                    string updateQuery = "Update " + tblName + " set ";
              //                    for (int i = 0; i < dt.Columns.Count; i++)
              //                    {
              //                        if (dt.Columns[i].ColumnName.ToString() != "Patient ID")
              //                        {
              //                            if (checkDatatype(dt.Columns[i].ColumnName, dtCols))
              //                            {
              //                                updateQuery += "[" + dt.Columns[i].ColumnName + "]=" + (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + convertDate(dt.Rows[j][i].ToString()) + "'") + " , ";
              //                            }
              //                            else
              //                            {
              //                                updateQuery += "[" + dt.Columns[i].ColumnName + "]=" + (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + dt.Rows[j][i].ToString().Replace("'", "''") + "'") + " , ";
              //                            }
              //                        }
              //                        else
              //                        {
              //                            whereCondition = " where [" + dt.Columns[i].ColumnName.ToString() + "]='" + dt.Rows[j][i].ToString().Trim() + "'";
              //                        }
              //                    }

              //                    updateQuery = updateQuery.Substring(0, updateQuery.Length - 3) + whereCondition;
              //                    SqlCommand cmd = new SqlCommand(updateQuery, con);
              //                    if (Convert.ToInt32(cmd.ExecuteNonQuery().ToString()) > 0)
              //                    {
              //                        updateCount++;
              //                        continue; //Increment j+1
              //                    }

              //                }
              //                #endregion

              //                if (ddlData.SelectedIndex == 3)
              //                {
              //                    string query2 = query;
              //                    query2 += " select ";
              //                    for (int i = 0; i < dt.Columns.Count; i++)
              //                    {
              //                        if (checkDatatype(dt.Columns[i].ColumnName, dtCols))
              //                        {

              //                            query2 += (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + convertDate2(dt.Rows[j][i].ToString()) + "'") + " as [" + dt.Columns[i].ColumnName + "],";
              //                        }
              //                        else
              //                        {
              //                            query2 += (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + dt.Rows[j][i].ToString().Replace("'", "''") + "'") + " as [" + dt.Columns[i].ColumnName + "],";
              //                        }
              //                    }
              //                    if (ddlData.SelectedIndex == 1)
              //                    {
              //                        query2 += "'" + rtbPatientID.Text.Trim().Replace("'", "''") + "' as [Patient ID],";
              //                    }
              //                    query2 = query2.Substring(0, query2.Length - 1);

              //                    SqlCommand cmd1 = new SqlCommand(query2, con);
              //                    cmd1.ExecuteNonQuery();
              //                    count++;

              //                }
              //                else
              //                {
              //                    string query2 = query;
              //                    query2 += " select ";
              //                    for (int i = 0; i < dt.Columns.Count; i++)
              //                    {
              //                        if (checkDatatype(dt.Columns[i].ColumnName, dtCols))
              //                        {

              //                            query2 += (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + convertDate(dt.Rows[j][i].ToString()) + "'") + " as [" + dt.Columns[i].ColumnName + "],";
              //                        }
              //                        else
              //                        {
              //                            query2 += (dt.Rows[j][i].ToString().Trim() == "" ? "NULL" : "'" + dt.Rows[j][i].ToString().Replace("'", "''") + "'") + " as [" + dt.Columns[i].ColumnName + "],";
              //                        }
              //                    }
              //                    if (ddlData.SelectedIndex == 1)
              //                    {
              //                        query2 += "'" + rtbPatientID.Text.Trim().Replace("'", "''") + "' as [Patient ID],";
              //                    }
              //                    query2 = query2.Substring(0, query2.Length - 1);

              //                    SqlCommand cmd1 = new SqlCommand(query2, con);
              //                    cmd1.ExecuteNonQuery();
              //                    count++;
              //                }
              //            }


              //            lblStatus.Text = count.ToString() + " Records Imported";

              //            if (ddlData.SelectedIndex == 3 && dt.Rows.Count > 0)
              //            {
              //                NoofRecordsDeleted = DeleteDuplicateRMData();
              //                if (NoofRecordsDeleted > 0)
              //                {
              //                    lblStatus.Text = (dt.Rows.Count - NoofRecordsDeleted).ToString() + " Records Imported, " + NoofRecordsDeleted.ToString() + " Duplicate Records Deleted";
              //                }
              //            }

              //            if (ddlData.SelectedIndex == 0 && updateCount > 0)
              //            {
              //                lblStatus.Text += ", " + updateCount.ToString() + " Records Updated";
              //            }

              //            //Start Display Duplicate Records on Screen
              //            //lblStatus.Text = count.ToString() + "/" + (dt.Rows.Count - emptyRows).ToString() + " Records Imported Successfully";
              //            //if (dt.Rows.Count > 0)
              //            //{

              //            //    lblStatus.Text += ", Contains " + dupCount.ToString() + " Duplicate Records.";
              //            //}
              //            //End Display Duplicate Records on Screen

              //            lblStatus.ForeColor = System.Drawing.Color.Green;

              //            filePath = "~/Files/" + fileName;
              //            fileType = ddlData.SelectedItem.Text.ToString() + "(" + lblStatus.Text + ")";

              //            CreateImportLog(fileName, filePath, fileType);

              //            ViewState["DocData"] = dt;
              //            ddlData.ClearSelection();
              //            rtbPatientID.Visible = false;
              //            BindGrid();

              //        }

              //    }
              //    else
              //    {
              //        lblStatus.Text = "Please select a File. ";
              //        lblStatus.ForeColor = System.Drawing.Color.Red;
              //    }
              //}
              //catch (Exception ex)
              //{
              //    lblStatus.Text = count + " Records Saved. \nError:" + ex.Message;
              //    lblStatus.ForeColor = System.Drawing.Color.Red;
              //}

              ///////////////////////////////////////////////////////////////////////////////////
              # endregion
          }
          public ActionResult ViewLogs(string paging)
          {
              if (!String.IsNullOrEmpty(paging))
              {
                  ViewBag.rowsPerPage = int.Parse(paging);
              }
              else
              {
                  // default value
                  ViewBag.rowsPerPage = 20;
              }

           

              DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
           
              List<DeviceAccess> lstDeviceTrackingLog = deviceDataAcessLayer.DeviceTrackingLog.ToList();

              ViewModelLogDetails viewModelLogDetails = new ViewModelLogDetails();

              viewModelLogDetails.RmDataLogDetails = deviceDataAcessLayer.DeviceTrackingLog.ToList();
              viewModelLogDetails.ExpandableDetails = deviceDataAcessLayer.ExpandablesDevicetrackingLog.ToList();
         
              return View(viewModelLogDetails);

             // return View(lstDeviceTrackingLog);

          }
          public ActionResult Customers()
          {
              //ViewBag.Message = "Your Customers page.";

              return View();
          }
          public ActionResult Devices()
          {
              //ViewBag.Message = "Your Devices page.";

              return View();
          }
          public ActionResult Patients()
          {
              // ViewBag.Message = "Your Patients page.";

              return View();
          }

          public Int64 DeleteDuplicateRMData()
          {
              Int64 isDeleted = 0;

              using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString))
              {
                  con.Open();
                  SqlCommand cmd = new SqlCommand("dbo.SP_CleanUpDuplicateRMData", con);
                  cmd.CommandType = CommandType.StoredProcedure;
                  cmd.CommandTimeout = 0;
                  Int64 rows = cmd.ExecuteNonQuery();
                  if (rows > 1)
                  {
                      isDeleted = rows;
                  }
              }
              return isDeleted;
          }
          private bool checkDatatype(string colName, DataTable dt)
          {
              for (int i = 0; i < dt.Rows.Count; i++)
              {
                  if (colName.ToLower().Trim() == dt.Rows[i]["COLUMN_NAME"].ToString().ToLower().Trim())
                  {
                      if (dt.Rows[i]["DATA_TYPE"].ToString().ToLower().Trim() == "datetime")
                          return true;
                      else
                          return false;
                  }
              }
              return false;
          }

          private static DataTable ProcessCSV(string fileName)
          {
              //Set up our variables
              string Feedback = string.Empty;
              string line = string.Empty;
              string[] strArray;
              DataTable dt = new DataTable();
              DataRow row;
              // work out where we should split on comma, but not in a sentence
              Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
              //Set the filename in to our stream
              StreamReader sr = new StreamReader(fileName);

              //Read the first line and split the string at , with our regular expression in to an array
              line = sr.ReadLine();
              strArray = r.Split(line);

              //For each item in the new split array, dynamically builds our Data columns. Save us having to worry about it.
              Array.ForEach(strArray, s => dt.Columns.Add(new DataColumn()));

              //Read each line in the CVS file until it’s empty
              while ((line = sr.ReadLine()) != null)
              {
                  row = dt.NewRow();

                  //add our current value to our data row
                  row.ItemArray = r.Split(line);
                  dt.Rows.Add(row);
              }

              //Tidy Streameader up
              sr.Dispose();

              //return a the new DataTable
              return dt;

          }

          private static DataTable GetDataTabletFromCSVFile(System.IO.Stream csv_file_path)
          {
              DataTable csvData = new DataTable();
              try
              {
                  using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                  {
                      csvReader.SetDelimiters(new string[] { "," });
                      csvReader.HasFieldsEnclosedInQuotes = true;


                      string[] colFields = csvReader.ReadFields();


                      while (colFields[0].Length > 0 && colFields.Length == 1)
                      {
                          colFields = csvReader.ReadFields();
                      }

                      while (colFields[0].Length > 0 && colFields[1].Length == 0 && colFields[2].Length == 0 && colFields[3].Length == 0)
                      {
                          colFields = csvReader.ReadFields();
                      }

                      while (colFields[0].Length == 0 && colFields[1].Length == 0 && colFields[2].Length == 0 && colFields[3].Length == 0)
                      {
                          colFields = csvReader.ReadFields();
                      }

                      foreach (string column in colFields)
                      {
                          DataColumn datecolumn = new DataColumn(column);
                          datecolumn.AllowDBNull = true;
                          csvData.Columns.Add(datecolumn);
                      }
                      while (!csvReader.EndOfData)
                      {
                          string[] fieldData = csvReader.ReadFields();

                          while (fieldData[0].Length > 0 && fieldData[1].Length == 0 && fieldData[2].Length == 0 && fieldData[3].Length == 0)
                          {
                              fieldData = csvReader.ReadFields();
                          }
                          //Making empty value as null
                          for (int i = 0; i < fieldData.Length; i++)
                          {
                              if (fieldData[i] == "")
                              {
                                  fieldData[i] = null;
                              }
                          }
                          csvData.Rows.Add(fieldData);
                      }

                  }
              }
              catch (Exception ex)
              {
              }
              return csvData;
          }

          private static String ProcessBulkCopy(DataTable dt)
          {
              string Feedback = string.Empty;
              string connString = ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString;
              DataTable dtCols = new DataTable();
              //make our connection and dispose at the end
              using (SqlConnection conn = new SqlConnection(connString))
              {
                  //make our command and dispose at the end
                  using (var copy = new SqlBulkCopy(conn))
                  {

                      //Open our connection
                      conn.Open();

                      ///Set target table and tell the number of rows
                      copy.DestinationTableName = "tblRMData";// "BulkImportDetails";
                      copy.BatchSize = dt.Rows.Count;
                     
                      try
                      {    ////Send it to the server
                          copy.WriteToServer(dt);
                          Feedback = "Upload complete";

                          //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DeviceContext"].ConnectionString))
                          //{
                          //    con.Open();
                          //    string query = "SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" + tblName + "' AND ORDINAL_POSITION<>1";
                          //    SqlDataAdapter da = new SqlDataAdapter(query, con);
                          //    da.Fill(dtCols);
                          //}
                          //string colNames = "";
                          //for (int i = 0; i < dtCols.Rows.Count; i++)
                          //{
                          //    bool colFound = false;
                          //    for (int j = 0; j < dt.Columns.Count; j++)
                          //    {
                          //        if (dt.Columns[j].ColumnName.ToString().ToLower().Trim().Replace("#", ".")
                          //            == dtCols.Rows[i]["COLUMN_NAME"].ToString().ToLower().Trim().Replace("#", "."))
                          //        {
                          //            colFound = true;
                          //            break;
                          //        }
                          //    }
                          //    if (!colFound)
                          //        colNames += dtCols.Rows[i]["COLUMN_NAME"].ToString() + ",";
                          //}
                          
                      }
                      catch (Exception ex)
                      {
                          Feedback = ex.Message;
                      }
                  }
              }

              return Feedback;
          }

       
    }
}
