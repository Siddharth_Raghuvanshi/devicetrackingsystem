﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAcessLayer;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.Security;
using DeviceTrackingSystem.Models;
using System.Net;

namespace DeviceTrackingSystem.Controllers
{
    public class DeviceController : Controller
    {
        DeviceTrackingEntities db = new DeviceTrackingEntities();
        DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
        [HttpGet]
        // GET: /Device/
        [Authorize]
        public ActionResult DeviceIndex(string paging)
        {
            if (!String.IsNullOrEmpty(paging))
            {                
                ViewBag.rowsPerPage = int.Parse(paging);
            }
            else
            {
                // default value
                ViewBag.rowsPerPage = 20;
            }
            DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
            List<DeviceAccess> lstDevice = deviceDataAcessLayer.DeviceData.ToList();
            if (HttpContext.Request.QueryString.Count == 0 || HttpContext.Request.QueryString.Count > 0)
            {
                string showAll = string.Empty;
                if (HttpContext.Request.QueryString["ShowAll"] != null)
                    showAll = HttpContext.Request.QueryString["ShowAll"].ToString().Trim();
                if (showAll == "true")
                {
                    lstDevice = deviceDataAcessLayer.DeviceData.ToList();
                }
                else if (showAll == "false")
                {
                    lstDevice = deviceDataAcessLayer.DeviceData.Where(d => d.Archived == true).ToList();
                }
                else
                {
                    lstDevice = deviceDataAcessLayer.DeviceData.Where(d => d.Archived == false).ToList();
                }
            }
            if (HttpContext.Request.QueryString.Count > 0)
            {
                string SearchKey = string.Empty;
                if (HttpContext.Request.QueryString["SearchKey"] != null)
                    SearchKey = HttpContext.Request.QueryString["SearchKey"].ToString().Trim();
                string ParentSearchKey = string.Empty;
                if (HttpContext.Request.QueryString["ParentSearchKey"] != null)
                    ParentSearchKey = HttpContext.Request.QueryString["ParentSearchKey"].ToString().Trim();
                if (!string.IsNullOrEmpty(SearchKey) && string.IsNullOrEmpty(ParentSearchKey))
                {
                    lstDevice = lstDevice.Where(i => i.Part_ID.Contains(SearchKey) || i.Serial_Number.Contains(SearchKey)).ToList();
                }
                else if (string.IsNullOrEmpty(SearchKey) && !string.IsNullOrEmpty(ParentSearchKey))
                {
                    lstDevice = lstDevice.Where(i => i.Parent_ID.Contains(ParentSearchKey) || i.ParentSerialNumber.Contains(ParentSearchKey)).ToList();
                }
                else if (!string.IsNullOrEmpty(SearchKey) && !string.IsNullOrEmpty(ParentSearchKey))
                {
                    lstDevice = lstDevice.Where(i => i.Part_ID.Contains(SearchKey) || i.Serial_Number.Contains(SearchKey) || i.Parent_ID.Contains(ParentSearchKey) || i.ParentSerialNumber.Contains(ParentSearchKey)).ToList();
                }
            }
            return View(lstDevice);

          
        }        

        //
        // GET: /Device/Details/5
        //[Authorize]
        //public ActionResult Details(int id)
        //{
        //    DeviceDataAcessLayer objDetails = new DeviceDataAcessLayer();
        //    //Device device = objDetails.Device.FirstOrDefault(emp => emp.ID == id);
        //    DeviceAccess device = objDetails.DeviceData.FirstOrDefault(emp => emp.ID == id);
        //    return View(device);
        //}
        [Authorize(Roles = "ReadWrite")]
        public ActionResult Details(int id = 0)
        {
            DataAcessLayer.DeviceAccess.ViewModelDeviceDetails objviewModeldeviceDetails = new DataAcessLayer.DeviceAccess.ViewModelDeviceDetails();
            objviewModeldeviceDetails.deviceDetails = deviceDataAcessLayer.DeviceDetailDAL().Where(d => d.ID == id).ToList();
            objviewModeldeviceDetails.deviceParentDetails = deviceDataAcessLayer.DeviceParentDetailsDAL().Where(d => d.ID == id).ToList();
            objviewModeldeviceDetails.deviceLocationHistoryDetails = deviceDataAcessLayer.DeviceLocationHistoryDetailsDAL().Where(d => d.DeviceID == id).ToList();
            //objviewModeldeviceDetails.deviceChildDetails = deviceDataAcessLayer.DeviceChildDetailsDAL().Where(d => d.ID == id).ToList();
            objviewModeldeviceDetails.deviceChildDetails = deviceDataAcessLayer.DeviceSubChildDetailsDAL(id).ToList();
           return View(objviewModeldeviceDetails);
        }        

        //
        // GET: /Device/Create
           [Authorize(Roles = "ReadWrite")]
        public ActionResult Create()
        {
            // Class1 dbconnection = new Class1();
            DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
            List<DataAcessLayer.DeviceAccess.DeviceDetails> pcontent = new List<DeviceAccess.DeviceDetails>();
            {
                //Get all page content from TblHome table
                pcontent = deviceDataAcessLayer.DeviceDetailDAL();

            };
            List<SelectListItem> DeviceList = new List<SelectListItem>();          
            foreach (var item in pcontent)
            {
                DeviceList.Add(new SelectListItem
                {
                    Text = item.PartID,
                    Value = item.ID.ToString()
                });
            }

            ViewBag.NameList = DeviceList;
            return View();     
        }

        //
        // POST: /Device/Create
        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                string Part_ID = Request.Form["Part_ID"];
                string Serial_Number = Request.Form["Serial_Number"];
                string Status = Request.Form["StatusDropDown"];
                int Parent_ID = Convert.ToInt32(Request.Form["ID"]);               

                string updatedby = User.Identity.Name;
                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isSaved = deviceLayer.SaveDevicesDetails(Part_ID, Serial_Number, Status, Parent_ID, updatedby);
                    if (isSaved)
                    {
                        TempData["message"] = "Record Successfully Saved";

                    }
                    else
                    {
                        TempData["message"] = "Record Not Saved";
                    }

                }
                return RedirectToAction("Create");

            }
            catch
            {
                return View();
            }
        }

        public ActionResult CreateDeviceLocation()
        {
            // Class1 dbconnection = new Class1();
            DeviceDataAcessLayer deviceDataAcessLayer = new DeviceDataAcessLayer();
            List<DataAcessLayer.DeviceAccess.DeviceDetails> pcontent = new List<DeviceAccess.DeviceDetails>();
            {
                //Get all page content from TblHome table
                pcontent = deviceDataAcessLayer.DeviceDetailDAL();

            };
            List<SelectListItem> DeviceList = new List<SelectListItem>();
            foreach (var item in pcontent)
            {
                DeviceList.Add(new SelectListItem
                {
                    Text = item.PartID,
                    Value = item.ID.ToString()
                });
            }

            ViewBag.NameList = DeviceList;

            List<DataAcessLayer.DeviceAccess.Customers> pcontent1 = new List<DeviceAccess.Customers>();
            {
                //Get all page content from TblHome table
                pcontent1 = deviceDataAcessLayer.CustomersListDAL();

            };
            List<SelectListItem> CustomersList = new List<SelectListItem>();
            foreach (var item in pcontent1)
            {
                CustomersList.Add(new SelectListItem
                {
                    Text = item.CustomerName,
                    Value = item.CustomerID.ToString()
                });
            }

            ViewBag.CustomerList = CustomersList;
            return View();
        }

        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
        public ActionResult CreateDeviceLocation(FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                int Device_ID = Convert.ToInt32(Request.Form["ID"]);
                int Customer_ID = Convert.ToInt32(Request.Form["CustomerID"]);             
                string SO_ID = Request.Form["SO_ID"];
                string CurrentLoc = Request.Form["StatusDropDown"];

                string updatedby = User.Identity.Name;
                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isSaved = deviceLayer.SaveDeviceLocationDetails(Device_ID, Customer_ID, SO_ID, CurrentLoc, updatedby);
                    if (isSaved)
                    {
                        TempData["message"] = "Record Successfully Saved";

                    }
                    else
                    {
                        TempData["message"] = "Record Not Saved";
                    }

                }
                return RedirectToAction("CreateDeviceLocation");

            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult DeviceCreate(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Device/Edit/5
       [Authorize(Roles = "ReadWrite")]
       [HttpGet]
       public ActionResult Edit(int id = 0, int CustId = 0, int DeviceLocId = 0, string status = "", int ParentID = 0, int Pid = 0, string ParentpartId = "", string psn = "")
        {
            Device device = new Device();
            Customer customer = new Customer();
            //DeviceAccess device = new DeviceAccess();
            DeviceDataAcessLayer devicDAL = new DeviceDataAcessLayer();

            try
            {
                device = db.Devices.Find(id);
                if (device == null)
                {
                    return HttpNotFound();
                }
                ViewBag.ParentID = new SelectList(deviceDataAcessLayer.DeviceDataDistinctParentID, "Parent_ID", "Parent_ID", ParentpartId.Trim());
                ViewBag.status = new SelectList(db.Devices, "Status", "Status", status);
                ViewBag.parentSerialNumber = new SelectList(deviceDataAcessLayer.DeviceData, "PID", "ParentSerialNumber", Pid);
                ViewBag.customerName = new SelectList(db.Customers, "ID", "Customer_Name", CustId);

                bool IsArchived = devicDAL.GetDeviceIsArchived(id);
                ViewBag.Isarchived = IsArchived;
                TempData["DeviceLocId"] = DeviceLocId;
                TempData["IsArchived"] = IsArchived;
                TempData["psn"] = psn;

                ViewData["psn"] = psn;
                GetSerialNumber(ParentpartId);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            return View(device);
        }

       public JsonResult GetSerialNumber(string id)
       {
           List<DataAcessLayer.DeviceAccess.DeviceDetails> SerialNumbers = new List<DataAcessLayer.DeviceAccess.DeviceDetails>();
           List<DataAcessLayer.DeviceAccess.DeviceDetails> devices = new List<DataAcessLayer.DeviceAccess.DeviceDetails>();
           DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
           ViewBag.SerialNumberByPartID = new SelectList(deviceLayer.GetSerialNumberByPartID(id), "Serial_Number", "Serial_Number");
           SerialNumbers = deviceLayer.GetSerialNumberByPartID(id);
           return Json(new SelectList(SerialNumbers, "Serial_Number", "Serial_Number"));
       }
        
        [Authorize(Roles = "ReadWrite")]
        [HttpPost]
       public ActionResult Edit(int id, FormCollection form)
        {
            try
            {
                DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                // TODO: Add update logic here
                string status = Request.Form["Status"];               

                string ParentID = Request.Form["ParentID"];
                int p = ParentID.IndexOf(",");
                string ParentIDnew = (p >= 0) ? ParentID.Substring(0, p) : ParentID;
            
                string SerialNumberParentId = "";
                string SerialNo = Request.Form["psn"];
                if (Request.Form["psn"].ToString()=="")
                {
                    SerialNo = Convert.ToString(TempData["psn"]);
                }

                int PID = deviceLayer.GetParentIdByParentandSerialNum(ParentID,SerialNo);
                string archived = Request.Form["templateId"];
                int a = archived.IndexOf(",");
                string archivednew = (a >= 0) ? archived.Substring(0, a) : archived;


                SerialNumberParentId = SerialNo; //SerialNumberParentId.Substring(0, SerialNumberParentId.IndexOf(','));
                
                int CustomerID=0;
                if (Request.Form["Customer_ID"]!="")
                {
                     CustomerID = Convert.ToInt32(Request.Form["Customer_ID"]);
                }
               
                var updatedby = User.Identity.Name;
                if (ModelState.IsValid)
                {

                    bool isUpdated = deviceLayer.SaveDevices(status, PID, SerialNumberParentId, CustomerID, id, Convert.ToInt32(TempData["DeviceLocId"]), updatedby, archivednew);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        return RedirectToAction("DeviceIndex");
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("DeviceIndex");
                    }
                    
                }
                return RedirectToAction("DeviceIndex");
               
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Device/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            return View();
        }
        [Authorize(Roles = "ReadWrite")]
        public ActionResult DeleteDevice(int id = 0)
        {
            if (ModelState.IsValid)
            {
                DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                var updatedby = User.Identity.Name;
                bool IsDeleted = deviceLayer.DeleteDeviceRecord(id, updatedby);
                if (IsDeleted)
                {
                    TempData["message"] = "Record Successfully Deleted";
                }
                return RedirectToAction("DeviceIndex");
            }
            return View();
        }
        //
        // POST: /Device/Delete/5        

        public ActionResult SearchText()
        {

            return View();
        }

        [Authorize(Roles = "ReadWrite")]
        [HttpGet]
        public ActionResult DeviceParentDetails(int id = 0, string partID = "", string serialNumber = "", string parentPartID = "", string parentSerialNumber = "")
        {
            ViewBag.id = id;
            ViewBag.partID = partID.ToString();
            ViewBag.serialNumber = serialNumber.ToString();
            ViewBag.parentPartID = parentPartID.ToString();
            ViewBag.parentSerialNumber = parentSerialNumber.ToString();
            return View();
        }
        [Authorize(Roles = "ReadWrite")]
        [HttpGet]
        public ActionResult DeviceLocationHistoryDetails(int deviceID = 0, string customerName = "", string salesOrderID = "", string deviceLocDate = "")
        {
            ViewBag.deviceID = deviceID;
            ViewBag.customerName = customerName.ToString();
            ViewBag.salesOrderID = salesOrderID.ToString();
            ViewBag.deviceLocDate = deviceLocDate.ToString();
            return View();
        }
        [Authorize(Roles = "ReadWrite")]
        [HttpGet]
        public ActionResult DeviceChildDetails(int id = 0, string partID = "", string serialNumber = "", string status = "", string parentPartID = "", string parentSerialNumber = "",int pid=0)
        {
            ViewBag.id = pid;
            ViewBag.partID = partID.ToString();
            ViewBag.serialNumber = serialNumber.ToString();
            ViewBag.status = status.ToString();
            ViewBag.parentPartID = parentPartID.ToString();
            ViewBag.parentSerialNumber = parentSerialNumber.ToString();
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "ReadWrite")]
        public ActionResult DeviceParentDetailsEdit(int id = 0,int partID = 0)
        {
            List<DataAcessLayer.DeviceAccess.DeviceDetails> objdeviceDetails = new List<DataAcessLayer.DeviceAccess.DeviceDetails>();
            ViewBag.id1 = id;
            try
            {
                objdeviceDetails = deviceDataAcessLayer.DeviceDetailDAL();
                ViewBag.PartID = new SelectList(deviceDataAcessLayer.DeviceDetailDAL(), "ID", "PartID", partID);
                ViewBag.serialNumber = new SelectList(deviceDataAcessLayer.DeviceDetailDAL(), "ID", "SerialNumber", partID);
                ViewBag.parentPartID = new SelectList(deviceDataAcessLayer.DeviceDetailDAL(), "ID", "ParentPartID", partID);
                ViewBag.parentSerialNumber = new SelectList(deviceDataAcessLayer.DeviceDetailDAL(), "ID", "ParentSerialNumber", partID);
               
            }

            catch (Exception ex)
            {

            }
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "ReadWrite")]
        public ActionResult DeviceParentDetailsEdit(int id, FormCollection collection, int partID=0)
        {
            try
            {
                // TODO: Add update logic here
                string PID = Request.Form["ID"];
                string newpID = PID.Split(',')[0];
                int NewPartId = Convert.ToInt32(newpID);
                int PartId = Convert.ToInt32(partID);
               // ViewBag.id = PartId;
                ViewBag.id = id;
                var updatedby = User.Identity.Name;


                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SaveDevicesParentDevice(NewPartId,PartId, updatedby);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        return RedirectToAction("Details", new { id = PartId, partID = PartId });
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("Details", new { id = PartId, partID = PartId });
                    }

                }
                return RedirectToAction("Details", new { id = PartId, partID = PartId });

            }
            catch
            {
                return View();
            }
        }    

        [HttpGet]
        [Authorize(Roles = "ReadWrite")]
        public ActionResult DeviceLocationHistoryDetailsEdit(int id = 0, int CustId = 0, int deviceId=0)
        {
            List<DataAcessLayer.DeviceAccess.DeviceLocationHistoryDetails> objdeviceLocationHistoryDetails = new List<DataAcessLayer.DeviceAccess.DeviceLocationHistoryDetails>();
            ViewBag.id = deviceId;
            try
            {
                objdeviceLocationHistoryDetails = deviceDataAcessLayer.DeviceLocationHistoryDetailsDAL();
                ViewBag.customerName = new SelectList(deviceDataAcessLayer.DeviceLocationHistoryDetailsDAL(), "ID", "CustomerName", CustId);
                ViewBag.salesOrderID = new SelectList(deviceDataAcessLayer.DeviceLocationHistoryDetailsDAL(), "ID", "SalesOrderID", id);
                ViewBag.deviceLocDate = new SelectList(deviceDataAcessLayer.DeviceLocationHistoryDetailsDAL(), "ID", "DeviceLocDate", id);
            }

            catch (Exception ex)
            {

            }
            return View();
        }


        [HttpPost]
        [Authorize(Roles = "ReadWrite")]

        public ActionResult DeviceLocationHistoryDetailsEdit(int id, FormCollection collection, int deviceId = 0, int CustId = 0)
        {
            try
            {
                // TODO: Add update logic here
                string CID = Request.Form["ID"];
                string newCID = CID.Split(',')[0];
                int NewCustId = Convert.ToInt32(newCID);
                int DeviceId = Convert.ToInt32(deviceId);
                int CustomerId = Convert.ToInt32(CustId);
                ViewBag.id = DeviceId;
                var updatedby = User.Identity.Name;


                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SaveDevicesLocationHistoryDetails(NewCustId, DeviceId, CustomerId, updatedby);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        return RedirectToAction("Details", new { id = ViewBag.id });
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("Details", new { id = ViewBag.id });
                    }

                }
                return RedirectToAction("Details", new { id = ViewBag.id });

            }
            catch
            {
                return View();
            }
        }       
       

        [HttpGet]
        [Authorize(Roles = "ReadWrite")]
        public ActionResult DeviceChildDetailsEdit(int id = 0,int pid=0)
        {
            List<DataAcessLayer.DeviceAccess.DeviceChildDetails> objdeviceChildDetails = new List<DataAcessLayer.DeviceAccess.DeviceChildDetails>();
            ViewBag.id = pid;
            try
            {
                objdeviceChildDetails = deviceDataAcessLayer.DeviceChildDetailsDAL();
                ViewBag.partID = new SelectList(deviceDataAcessLayer.DeviceChildDetailsDAL(), "ID", "PartID", id);
                ViewBag.serialNumber = new SelectList(deviceDataAcessLayer.DeviceChildDetailsDAL(), "ID", "SerialNumber", id);
                ViewBag.status = new SelectList(deviceDataAcessLayer.DeviceChildDetailsDAL(), "ID", "Status", id);
                ViewBag.parentPartID = new SelectList(deviceDataAcessLayer.DeviceChildDetailsDAL(), "ID", "ParentPartID", id);
                ViewBag.parentSerialNumber = new SelectList(deviceDataAcessLayer.DeviceChildDetailsDAL(), "ID", "ParentSerialNumber", id);
            }

            catch (Exception ex)
            {

            }
            return View();
        }


        [HttpPost]
        [Authorize(Roles = "ReadWrite")]

        public ActionResult DeviceChildDetailsEdit(int id, FormCollection collection, int deviceId = 0, int CustId = 0)
        {
            try
            {
                // TODO: Add update logic here
                string CID = Request.Form["ID"];
                string newCID = CID.Split(',')[0];
                int NewCustId = Convert.ToInt32(newCID);
                int DeviceId = Convert.ToInt32(deviceId);
                int CustomerId = Convert.ToInt32(CustId);
                ViewBag.id = DeviceId;
                var updatedby = User.Identity.Name;


                if (ModelState.IsValid)
                {
                    DeviceDataAcessLayer deviceLayer = new DeviceDataAcessLayer();
                    bool isUpdated = deviceLayer.SaveDevicesChildDetails(NewCustId, DeviceId, CustomerId, updatedby);
                    if (isUpdated)
                    {
                        TempData["message"] = "Record Successfully Updated";
                        return RedirectToAction("Details", new { id = ViewBag.id });
                    }
                    {
                        TempData["message"] = "Error Occured!!";
                        return RedirectToAction("Details", new { id = ViewBag.id });
                    }

                }
                return RedirectToAction("Details", new { id = ViewBag.id });

            }
            catch
            {
                return View();
            }
        }
    }
}
